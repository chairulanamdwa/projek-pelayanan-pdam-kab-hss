<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbBukaTutupMeterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_buka_tutup_meter', function (Blueprint $table) {
            $table->increments('id_buktup_meter');
            $table->integer('id_pelanggan');
            $table->text('no_ds');
            $table->text('permintaan');
            $table->date('tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_buka_tutup_meter');
    }
}
