<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbTunggakanRekAirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_tunggakan_rek_air', function (Blueprint $table) {
            $table->increments('id_tunggakan');
            $table->integer('id_pelanggan');
            $table->text('no_ds');
            $table->text('tunggakan');
            $table->date('tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_tunggakan_rek_air');
    }
}
