<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbTeraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_tera', function (Blueprint $table) {
            $table->increments('id_tera');
            $table->integer('id_pelanggan');
            $table->text('no_ds');
            $table->date('tgl_keluhan');
            $table->date('tgl_perbaikan');
            $table->date('tgl_selesai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_tera');
    }
}
