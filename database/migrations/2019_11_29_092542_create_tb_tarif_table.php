<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbTarifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_tarif', function (Blueprint $table) {
            $table->increments('id_tarif');
            $table->text('deskripsi');
            $table->text('kode');
            // $table->text('pemakaian_m3');
            $table->text('tarip_m3');
            $table->text('minimum');
            $table->text('abonemen');
            $table->text('denda');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_tarif');
    }
}
