<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbKebocoranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_kebocoran', function (Blueprint $table) {
            $table->increments('id_kebocoran');
            $table->integer('id_pelanggan');
            $table->text('no_ds');
            $table->date('tgl_kebocoran');
            $table->date('tgl_perbaikan');
            $table->date('tgl_selesai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_kebocoran');
    }
}
