<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbTeraMeterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_tera_meter', function (Blueprint $table) {
            $table->increments('id_tera_meter');
            $table->integer('id_pelanggan');
            $table->date('tanggal_permintaan');
            $table->integer('id_teknisi');
            $table->date('tanggal_pemeriksaan');
            $table->text('hasil_pemeriksaan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_tera_meter');
    }
}
