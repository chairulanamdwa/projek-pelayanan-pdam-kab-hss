<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbKeluhanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_keluhan', function (Blueprint $table) {
            $table->increments('id_keluhan');
            $table->integer('id_pelanggan');
            $table->integer('id_teknisi')->nullable();
            $table->text('keluhan')->nullable();
            $table->string('hp', 255)->nullable();
            $table->string('jenis_keluhan', 255)->nullable();
            $table->string('model', 255)->nullable();
            $table->string('biaya', 255)->nullable();
            $table->date('tgl_pemeriksaan_perbaikan')->nullable();
            $table->text('hasil_pemeriksaan')->nullable();
            $table->enum('status_perbaikan', ['proses', 'selesai'])->default('proses');
            $table->date('tanggal_selesai')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_keluhan');
    }
}
