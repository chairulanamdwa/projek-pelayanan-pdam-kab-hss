<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbPelangganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pelanggan', function (Blueprint $table) {
            $table->increments('id_pelanggan');
            $table->text('no_pelanggan');
            $table->text('nama_pelanggan');
            $table->text('nik')->nullable();
            $table->text('alamat');
            $table->text('urutan')->nullable();
            $table->text('meter_lalu')->nullable();
            $table->text('catat_meter')->nullable();
            $table->text('gol_tarif')->nullable();
            $table->text('ukuran_pipa')->nullable();
            $table->text('sewa_meter')->nullable();
            $table->text('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pelanggan');
    }
}
