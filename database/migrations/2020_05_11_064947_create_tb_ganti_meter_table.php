<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbGantiMeterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_ganti_meter', function (Blueprint $table) {
            $table->increments('id_ganti_meter');
            $table->integer('id_pelanggan');
            $table->date('tgl_permintaan');
            $table->integer('id_teknisi');
            $table->date('tgl_perbaikan_ganti');
            $table->string('biaya_perbaikan', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_ganti_meter');
    }
}
