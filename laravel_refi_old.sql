-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Bulan Mei 2020 pada 15.48
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_refi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2019_11_29_073159_create_tb_pelanggan_table', 1),
(14, '2019_11_29_092542_create_tb_tarif_table', 1),
(15, '2019_11_29_234205_create_tb_tera_table', 1),
(16, '2019_11_30_025053_create_tb_kebocoran_table', 1),
(17, '2019_11_30_032100_create_tb_buka_tutup_meter_table', 1),
(18, '2019_11_30_061856_create_tb_tunggakan_rek_air_table', 1),
(40, '2020_05_09_184352_create_tb_keluhan_table', 2),
(41, '2020_05_10_134222_add_aktif_to_tb_buka_tutup_meter_table', 2),
(42, '2020_05_11_000217_create_tb_wilayah_table', 2),
(43, '2020_05_11_000429_add_id_wilayah_to_tb_pelanggan_table', 2),
(44, '2020_05_11_064947_create_tb_ganti_meter_table', 2),
(45, '2020_05_11_072101_create_tb_tera_meter_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_buka_tutup_meter`
--

CREATE TABLE `tb_buka_tutup_meter` (
  `id_buktup_meter` int(10) UNSIGNED NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `no_ds` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `permintaan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `aktif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_buka_tutup_meter`
--

INSERT INTO `tb_buka_tutup_meter` (`id_buktup_meter`, `id_pelanggan`, `no_ds`, `permintaan`, `tanggal`, `created_at`, `updated_at`, `aktif`) VALUES
(4, 1, '11', 'Sri weqw', '2020-05-11', '2020-05-11 19:44:52', '2020-05-11 19:52:37', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_ganti_meter`
--

CREATE TABLE `tb_ganti_meter` (
  `id_ganti_meter` int(10) UNSIGNED NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `tgl_permintaan` date NOT NULL,
  `id_teknisi` int(11) NOT NULL,
  `tgl_perbaikan_ganti` date NOT NULL,
  `biaya_perbaikan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kebocoran`
--

CREATE TABLE `tb_kebocoran` (
  `id_kebocoran` int(10) UNSIGNED NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `no_ds` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_kebocoran` date NOT NULL,
  `tgl_perbaikan` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_kebocoran`
--

INSERT INTO `tb_kebocoran` (`id_kebocoran`, `id_pelanggan`, `no_ds`, `tgl_kebocoran`, `tgl_perbaikan`, `tgl_selesai`, `created_at`, `updated_at`) VALUES
(1, 1, '1234', '2020-05-11', '2020-05-11', '2020-05-11', '2020-05-11 12:45:47', '2020-05-11 12:45:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_keluhan`
--

CREATE TABLE `tb_keluhan` (
  `id_keluhan` int(10) UNSIGNED NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `id_teknisi` int(11) DEFAULT NULL,
  `keluhan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_keluhan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biaya` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_pemeriksaan_perbaikan` date DEFAULT NULL,
  `hasil_pemeriksaan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_perbaikan` enum('proses','selesai') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'proses',
  `tanggal_selesai` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_keluhan`
--

INSERT INTO `tb_keluhan` (`id_keluhan`, `id_pelanggan`, `id_teknisi`, `keluhan`, `hp`, `jenis_keluhan`, `model`, `biaya`, `tgl_pemeriksaan_perbaikan`, `hasil_pemeriksaan`, `status_perbaikan`, `tanggal_selesai`, `created_at`, `updated_at`) VALUES
(1, 2, 2, NULL, '082217380171', 'Ganti Meter', NULL, '20000', '2020-05-11', NULL, 'selesai', '2020-05-11', '2020-05-11 18:49:57', '2020-05-11 19:21:58'),
(2, 1, 2, NULL, '082217380171', 'Tera Meter', NULL, NULL, '2020-05-11', 'Pelanggan Sangat Puas dengan Pelayanan', 'selesai', '2020-05-11', '2020-05-11 18:50:06', '2020-05-11 19:21:17'),
(3, 1, 2, NULL, NULL, 'Ganti Pipa', '1 00', '20000', NULL, NULL, 'selesai', '2020-05-11', '2020-05-11 19:22:38', '2020-05-11 19:24:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `id_pelanggan` int(10) UNSIGNED NOT NULL,
  `no_pelanggan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pelanggan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urutan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meter_lalu` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catat_meter` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gol_tarif` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ukuran_pipa` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sewa_meter` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_wilayah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`id_pelanggan`, `no_pelanggan`, `nama_pelanggan`, `nik`, `alamat`, `urutan`, `meter_lalu`, `catat_meter`, `gol_tarif`, `ukuran_pipa`, `sewa_meter`, `status`, `created_at`, `updated_at`, `id_wilayah`) VALUES
(1, '1Bsd.010001.01', 'Chairul Anam', '1234', 'Desa Tambak Bitin', '23123', '1231', 'dasd', '1', '1/2', '3123', '1', '2020-05-10 02:38:09', '2020-05-11 20:45:23', 1),
(2, '1Bsd.010002.01', '123', '123123', '123', '123', '123', '1234', '1', '123', '123', '1', '2020-05-11 10:16:22', '2020-05-11 20:45:29', 1),
(3, '1Bsd.010003.01', 'Rafi Azmi', '6306080104234', 'asdasd', 'asd', 'asd', 'asd', '1', '1234', 'asdasd', '1', '2020-05-11 11:36:56', '2020-05-11 20:45:35', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tarif`
--

CREATE TABLE `tb_tarif` (
  `id_tarif` int(10) UNSIGNED NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tarip_m3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `minimum` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `abonemen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `denda` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_tarif`
--

INSERT INTO `tb_tarif` (`id_tarif`, `deskripsi`, `kode`, `tarip_m3`, `minimum`, `abonemen`, `denda`, `created_at`, `updated_at`) VALUES
(1, 'Lorem', '1Bsd', 'Rp.1000\r\nRp.2000\r\nRp.3000', '20Kwh', 'lorem', '2000', '2020-05-09 04:19:56', '2020-05-09 04:19:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tera`
--

CREATE TABLE `tb_tera` (
  `id_tera` int(10) UNSIGNED NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `no_ds` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_keluhan` date NOT NULL,
  `tgl_perbaikan` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_tera`
--

INSERT INTO `tb_tera` (`id_tera`, `id_pelanggan`, `no_ds`, `tgl_keluhan`, `tgl_perbaikan`, `tgl_selesai`, `created_at`, `updated_at`) VALUES
(1, 1, '11312', '2020-05-11', '2020-05-11', '2020-05-11', '2020-05-11 09:40:29', '2020-05-11 09:42:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tera_meter`
--

CREATE TABLE `tb_tera_meter` (
  `id_tera_meter` int(10) UNSIGNED NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `tanggal_permintaan` date NOT NULL,
  `id_teknisi` int(11) NOT NULL,
  `tanggal_pemeriksaan` date NOT NULL,
  `hasil_pemeriksaan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tunggakan_rek_air`
--

CREATE TABLE `tb_tunggakan_rek_air` (
  `id_tunggakan` int(10) UNSIGNED NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `no_ds` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tunggakan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_tunggakan_rek_air`
--

INSERT INTO `tb_tunggakan_rek_air` (`id_tunggakan`, `id_pelanggan`, `no_ds`, `tunggakan`, `tanggal`, `created_at`, `updated_at`) VALUES
(1, 1, '11', 'Lorem', '2020-05-11', '2020-05-11 20:17:34', '2020-05-11 20:17:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_wilayah`
--

CREATE TABLE `tb_wilayah` (
  `id_wilayah` int(10) UNSIGNED NOT NULL,
  `nama_wilayah` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_wilayah`
--

INSERT INTO `tb_wilayah` (`id_wilayah`, `nama_wilayah`, `created_at`, `updated_at`) VALUES
(1, 'Gambah Dalam', '2020-05-11 18:50:14', '2020-05-11 18:50:14'),
(2, 'Gambah Luar', '2020-05-11 18:50:18', '2020-05-11 18:50:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('super admin','admin','user','teknisi') COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `role`, `gambar`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin 1', 'admin', '252279987.png', 'chairulanam@gmail.com', NULL, '$2y$10$rcBZ5.Wb3omy.p.Hk.D6NO6b.xo99Dk0o5m3UcTfiWhqWVfdHaif.', 'rrx1TBvBnX64lEzJeN38n0ra6P2xmgmIxOYMJJbJ8Ih976OjPvJSgYT4u7JS', '2020-05-09 03:47:54', '2020-05-10 02:48:53'),
(2, 'Muhammad Taufik', 'teknisi', 'default.png', 'teknisi@teknisi.com', NULL, '$2y$10$iahtPimGpR1craDIGcpz1.zhwmnP7b30c38aruOI1PTj3YelqUAAC', 'XzT7t2sK8MtRzRJZqXHbrQPb3oMQtz32KxVjyd1Vf5mBphE5p8RUsTws3rBu', '2020-05-10 03:13:57', '2020-05-10 03:13:57'),
(3, 'User1', 'user', 'default.png', 'user@user.com', NULL, '$2y$10$TLuNNBq1qScYB/AZTcyg.eol9LndI6ewa/KUZTlKaOOaUOI/rjNWi', 'TQWMsJ2zRyhNjgbPn1Z9yGrqQ1sKJMkrx9RJ6PYN6aFTTRa5lu9yr8iyURYZ', '2020-05-10 03:26:23', '2020-05-10 03:26:23'),
(4, 'Nami Chan', 'teknisi', 'default.png', 'teknisi2@teknisi.com', NULL, '$2y$10$t5c6K1pEfXkJw5MIRuUHJehNaKVIx1nOicOBhGg1ZNPdPBNnD2rH2', '2hC2j2kmf4ILA99i82efUrGpi5xxAnxtJGUst1G6h55lXQh96kXzEj5qg14f', '2020-05-10 13:26:16', '2020-05-10 13:26:16'),
(5, 'andre', 'user', 'default.png', 'rafiazmi@gmail.com', NULL, '$2y$10$LNbeqXnSJDPV28BcL28RSO7q30Vv2Ck0chhvWrx/ShrMe8W9OWJ4O', 'FMHZjxjhNbWomwHeJfttsAfUA6YIiktS1t0b2lkBEpMCNXC8EE4eaQfLXVHJ', '2020-05-10 18:55:23', '2020-05-10 18:55:23');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `tb_buka_tutup_meter`
--
ALTER TABLE `tb_buka_tutup_meter`
  ADD PRIMARY KEY (`id_buktup_meter`);

--
-- Indeks untuk tabel `tb_ganti_meter`
--
ALTER TABLE `tb_ganti_meter`
  ADD PRIMARY KEY (`id_ganti_meter`);

--
-- Indeks untuk tabel `tb_kebocoran`
--
ALTER TABLE `tb_kebocoran`
  ADD PRIMARY KEY (`id_kebocoran`);

--
-- Indeks untuk tabel `tb_keluhan`
--
ALTER TABLE `tb_keluhan`
  ADD PRIMARY KEY (`id_keluhan`);

--
-- Indeks untuk tabel `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indeks untuk tabel `tb_tarif`
--
ALTER TABLE `tb_tarif`
  ADD PRIMARY KEY (`id_tarif`);

--
-- Indeks untuk tabel `tb_tera`
--
ALTER TABLE `tb_tera`
  ADD PRIMARY KEY (`id_tera`);

--
-- Indeks untuk tabel `tb_tera_meter`
--
ALTER TABLE `tb_tera_meter`
  ADD PRIMARY KEY (`id_tera_meter`);

--
-- Indeks untuk tabel `tb_tunggakan_rek_air`
--
ALTER TABLE `tb_tunggakan_rek_air`
  ADD PRIMARY KEY (`id_tunggakan`);

--
-- Indeks untuk tabel `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  ADD PRIMARY KEY (`id_wilayah`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT untuk tabel `tb_buka_tutup_meter`
--
ALTER TABLE `tb_buka_tutup_meter`
  MODIFY `id_buktup_meter` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_ganti_meter`
--
ALTER TABLE `tb_ganti_meter`
  MODIFY `id_ganti_meter` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_kebocoran`
--
ALTER TABLE `tb_kebocoran`
  MODIFY `id_kebocoran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_keluhan`
--
ALTER TABLE `tb_keluhan`
  MODIFY `id_keluhan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  MODIFY `id_pelanggan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_tarif`
--
ALTER TABLE `tb_tarif`
  MODIFY `id_tarif` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_tera`
--
ALTER TABLE `tb_tera`
  MODIFY `id_tera` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_tera_meter`
--
ALTER TABLE `tb_tera_meter`
  MODIFY `id_tera_meter` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_tunggakan_rek_air`
--
ALTER TABLE `tb_tunggakan_rek_air`
  MODIFY `id_tunggakan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  MODIFY `id_wilayah` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
