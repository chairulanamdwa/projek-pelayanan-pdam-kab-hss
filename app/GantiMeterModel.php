<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GantiMeterModel extends Model
{
    protected $table = 'tb_tera';
    protected $fillable = ['id_pelanggan', 'no_ds', 'tgl_keluhan', 'tgl_perbaikan', 'tgl_selesai'];
}
