<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keluhan extends Model
{
    protected $table = 'tb_keluhan';
    protected $fillable = ['id_pelanggan', 'id_teknisi', 'keluhan', 'jenis_keluhan', 'biaya', 'model', 'hp', 'status', 'tanggal_selesai', 'tgl_pemeriksaan_perbaikan', 'hasil_pemeriksaan'];
}
