<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TunggakanRekAirModel extends Model
{
    protected $table = 'tb_tunggakan_rek_air';
    protected $fillable = ['id_pelanggan', 'no_ds', 'tunggakan', 'tanggal'];
}
