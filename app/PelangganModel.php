<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PelangganModel extends Model
{
    protected $table = 'tb_pelanggan';
    protected $fillable = ['no_pelanggan', 'nama_pelanggan', 'nik', 'alamat', 'urutan', 'meter_lalu', 'catat_meter', 'gol_tarif', 'ukuran_pipa', 'sewa_meter', 'status', 'id_wilayah'];
}
