<?php

namespace App\Http\Controllers;

use App\KebocoranModel;
use Illuminate\Http\Request;

class KebocoranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'kebocoran';
        $data['title'] = 'kebocoran';
        $data['kebocoran'] = KebocoranModel::whereMonth('tgl_kebocoran', date('m'))->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', 'tb_kebocoran.id_pelanggan')->get();
        return view('kebocoran.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sidebar'] = 'kebocoran';
        $data['title'] = 'kebocoran';
        return view('kebocoran.tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelanggan' => 'required',
            'no_ds' => 'required',
            'tgl_kebocoran' => 'required',
            'tgl_perbaikan' => 'required',
            'tgl_selesai' => 'required',
        ]);

        KebocoranModel::create($request->all());
        return redirect('kebocoran')->with('message', 'Data laporan kebocoran berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KebocoranModel  $kebocoranModel
     * @return \Illuminate\Http\Response
     */
    public function show(KebocoranModel $kebocoranModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KebocoranModel  $kebocoranModel
     * @return \Illuminate\Http\Response
     */
    public function edit(KebocoranModel $kebocoranModel, $id)
    {
        $data['sidebar'] = 'kebocoran';
        $data['title'] = 'kebocoran';
        $data['kebocoran'] = KebocoranModel::where('id_kebocoran', $id)->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_kebocoran.id_pelanggan')->first();
        return view('kebocoran.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KebocoranModel  $kebocoranModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KebocoranModel $kebocoranModel, $id)
    {
        $request->validate([
            'no_ds' => 'required',
            'tgl_kebocoran' => 'required',
            'tgl_perbaikan' => 'required',
            'tgl_selesai' => 'required',
        ]);


        KebocoranModel::where('id_kebocoran', $id)->update([
            'no_ds' => $request->no_ds,
            'tgl_kebocoran' => $request->tgl_kebocoran,
            'tgl_perbaikan' => $request->tgl_perbaikan,
            'tgl_selesai' => $request->tgl_selesai,
        ]);
        return redirect('kebocoran')->with('message', 'Data laporan kebocoran berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KebocoranModel  $kebocoranModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(KebocoranModel $kebocoranModel, $id)
    {
        KebocoranModel::where('id_kebocoran', $id)->delete();
        return redirect('kebocoran')->with('message', 'Data laporan kebocoran berhasil dihapus');
    }

    public function print()
    {
        $data['kebocoran'] = KebocoranModel::whereMonth('tgl_kebocoran', date('m'))->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', 'tb_kebocoran.id_pelanggan')->get();
        return view('kebocoran.print', $data);
    }
}
