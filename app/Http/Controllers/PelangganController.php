<?php

namespace App\Http\Controllers;

use App\PelangganModel;
use App\TarifModel;
use App\Wilayah;
use Illuminate\Http\Request;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'data master';
        $data['title'] = 'pelanggan';
        $data['pelanggan'] = PelangganModel::join('tb_tarif', 'tb_tarif.id_tarif', 'tb_pelanggan.gol_tarif')->get();
        return view('pelanggan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sidebar'] = 'data master';
        $data['title'] = 'pelanggan';
        $data['wilayah'] = Wilayah::orderBy('nama_wilayah', 'ASC')->get();
        $data['tarif'] = TarifModel::all();
        return view('pelanggan.tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_pelanggan' => 'required',
            'nik' => 'required',
            'alamat' => 'required',
            'urutan' => 'required',
            'meter_lalu' => 'required',
            'catat_meter' => 'required',
            'gol_tarif' => 'required',
            'ukuran_pipa' => 'required',
            'sewa_meter' => 'required',
            'id_wilayah' => 'required',
        ]);



        $kode = PelangganModel::orderBy('no_pelanggan', 'DESC')->first();
        $golongan = TarifModel::where('id_tarif', $request->gol_tarif)->first();

        if (!$kode) {
            $auto = "10001";
        } else {
            $no_pelanggan = $kode->no_pelanggan;
            $explode = explode('.', $kode->no_pelanggan);
            $auto = (int) $explode[1] + 1;
        }
        $no_pelanggan = $golongan->kode . '.0' . $auto . '.01';

        PelangganModel::create([
            'no_pelanggan' => $no_pelanggan,
            'nama_pelanggan' => $request->nama_pelanggan,
            'nik' => $request->nik,
            'alamat' => $request->alamat,
            'urutan' => $request->urutan,
            'meter_lalu' => $request->meter_lalu,
            'catat_meter' => $request->catat_meter,
            'gol_tarif' => $request->gol_tarif,
            'ukuran_pipa' => $request->ukuran_pipa,
            'sewa_meter' => $request->sewa_meter,
            'status' => $request->status,
            'id_wilayah' => $request->id_wilayah,
        ]);

        return redirect('pelanggan')->with('message', 'Data pelanggan berhasil didaftarkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PelangganModel  $pelangganModel
     * @return \Illuminate\Http\Response
     */
    public function show(PelangganModel $pelangganModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PelangganModel  $pelangganModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PelangganModel $pelangganModel, $id)
    {
        $data['sidebar'] = 'data master';
        $data['title'] = 'pelanggan';
        $data['tarif'] = TarifModel::all();
        $data['wilayah'] = Wilayah::orderBy('nama_wilayah', 'ASC')->get();
        $data['pelanggan'] = PelangganModel::where('id_pelanggan', $id)->first();
        return view('pelanggan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PelangganModel  $pelangganModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PelangganModel $pelangganModel, $id)
    {
        $request->validate([
            'nama_pelanggan' => 'required',
            'nik' => 'required',
            'alamat' => 'required',
            'urutan' => 'required',
            'meter_lalu' => 'required',
            'catat_meter' => 'required',
            'gol_tarif' => 'required',
            'ukuran_pipa' => 'required',
            'sewa_meter' => 'required',
            'id_wilayah' => 'required',
        ]);

        $kode = PelangganModel::where('id_pelanggan', $id)->first();
        $golongan = TarifModel::where('id_tarif', $request->gol_tarif)->first();

        $no_pelanggan = $kode->no_pelanggan;
        $explode = explode('.', $no_pelanggan);
        $no_pelanggan = $golongan->kode . '.' . $explode[1] . '.01';

        PelangganModel::where('id_pelanggan', $id)->update([
            'no_pelanggan' => $no_pelanggan,
            'nama_pelanggan' => $request->nama_pelanggan,
            'nik' => $request->nik,
            'alamat' => $request->alamat,
            'urutan' => $request->urutan,
            'meter_lalu' => $request->meter_lalu,
            'catat_meter' => $request->catat_meter,
            'gol_tarif' => $request->gol_tarif,
            'ukuran_pipa' => $request->ukuran_pipa,
            'sewa_meter' => $request->sewa_meter,
            'status' => $request->status,
            'id_wilayah' => $request->id_wilayah,
        ]);

        return redirect('pelanggan')->with('message', 'Data pelanggan berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PelangganModel  $pelangganModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(PelangganModel $pelangganModel, $id)
    {
        PelangganModel::where('id_pelanggan', $id)->delete();
        return redirect('pelanggan')->with('message', 'Data pelanggan berhasil dihapus');
    }

    public function print()
    {
        $data['pelanggan'] = PelangganModel::join('tb_tarif', 'tb_tarif.id_tarif', 'tb_pelanggan.gol_tarif')->get();
        return view('pelanggan.print', $data);
    }
}
