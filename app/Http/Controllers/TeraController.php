<?php

namespace App\Http\Controllers;

use App\Keluhan;
use Illuminate\Http\Request;

class TeraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'Ganti Meter';
        $data['title'] = 'Ganti Meter';
        $data['tera'] = Keluhan::where('jenis_keluhan', 'Tera Meter')
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
            ->get();
        return view('tera-meter.index', $data);
    }

    public function print()
    {
        $data['tera'] = Keluhan::where('jenis_keluhan', 'Tera Meter')
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
            ->get();
        return view('tera-meter.print', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
