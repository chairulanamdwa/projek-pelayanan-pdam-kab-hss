<?php

namespace App\Http\Controllers;

use App\GantiMeterModel;
use App\Keluhan;
use Illuminate\Http\Request;

class GantiMeterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'Ganti Meter';
        $data['title'] = 'Ganti Meter';
        $data['gantiMeter'] = Keluhan::where('jenis_keluhan', 'Ganti Meter')
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
            ->get();
        return view('ganti-meter.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sidebar'] = 'tera';
        $data['title'] = 'tera';
        return view('ganti-meter.tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelanggan' => 'required',
            'no_ds' => 'required',
            'tgl_keluhan' => 'required',
            'tgl_perbaikan' => 'required',
            'tgl_selesai' => 'required',
        ]);

        GantiMeterModel::create($request->all());
        return redirect()->back()->with('message', 'Data tera berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GantiMeterModel  $gantiMeterModel
     * @return \Illuminate\Http\Response
     */
    public function show(GantiMeterModel $gantiMeterModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GantiMeterModel  $gantiMeterModel
     * @return \Illuminate\Http\Response
     */
    public function edit(GantiMeterModel $gantiMeterModel, $id)
    {
        $data['sidebar'] = 'tera';
        $data['title'] = 'tera';
        $data['tera'] = GantiMeterModel::where('id_tera', $id)->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_tera.id_pelanggan')->first();
        return view('ganti-meter.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GantiMeterModel  $gantiMeterModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GantiMeterModel $gantiMeterModel, $id)
    {
        $request->validate([
            'no_ds' => 'required',
            'tgl_keluhan' => 'required',
            'tgl_perbaikan' => 'required',
            'tgl_selesai' => 'required',
        ]);

        GantiMeterModel::where('id_tera', $id)->update([
            'no_ds' => $request->no_ds,
            'tgl_keluhan' => $request->tgl_keluhan,
            'tgl_perbaikan' => $request->tgl_perbaikan,
            'tgl_selesai' => $request->tgl_selesai,
        ]);
        return redirect()->back()->with('message', 'Data tera berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GantiMeterModel  $gantiMeterModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(GantiMeterModel $gantiMeterModel, $id)
    {
        GantiMeterModel::where('id_tera', $id)->delete();
        return redirect()->back()->with('message', 'Data tera berhasil dihapus');
    }

    public function print()
    {
        $data['gantiMeter'] = Keluhan::where('jenis_keluhan', 'Ganti Meter')
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
            ->get();
        return view('ganti-meter.print', $data);
    }
}
