<?php

namespace App\Http\Controllers;

use App\BukaTutupMeterModel;
use App\GantiMeterModel;
use App\KebocoranModel;
use App\Keluhan;
use App\PelangganModel;
use App\TunggakanRekAirModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function dashboard()
    {
        $data['sidebar'] = 'Dashboard';
        $data['title'] = 'Dashboard';
        $data['pelanggan'] = PelangganModel::whereMonth('created_at', date('m'))->count();
        $data['tera'] = GantiMeterModel::whereMonth('created_at', date('m'))->count();
        $data['kebocoran'] = KebocoranModel::whereMonth('created_at', date('m'))->count();
        $data['btm'] = BukaTutupMeterModel::whereMonth('created_at', date('m'))->count();
        $data['tra'] = TunggakanRekAirModel::whereMonth('created_at', date('m'))->count();

        $data['pbwilayah'] = PelangganModel::join('tb_wilayah', 'tb_wilayah.id_wilayah', '=', 'tb_pelanggan.id_wilayah')
            ->select(DB::raw('count(nama_wilayah) as jumlah_wil,nama_wilayah'))
            ->groupBy('nama_wilayah')
            ->get();

        $data['seringRusak'] = Keluhan::join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->join('tb_wilayah', 'tb_wilayah.id_wilayah', '=', 'tb_pelanggan.id_wilayah')
            ->select(DB::raw('count(nama_wilayah) as jumlah_wil,nama_wilayah'))
            ->groupBy('nama_wilayah')
            ->get();

        return view('pages.dashboard.index', $data);
    }

    public function menuLayanan()
    {
        $data['sidebar'] = 'menu';
        $data['title'] = 'menu';
        return view('pages.menu-layanan.index', $data);
    }
}
