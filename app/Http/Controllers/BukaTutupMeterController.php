<?php

namespace App\Http\Controllers;

use App\BukaTutupMeterModel;
use Illuminate\Http\Request;

class BukaTutupMeterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'buka tutup meter';
        $data['title'] = 'buka tutup meter';
        $data['buka'] = BukaTutupMeterModel::where('aktif', 1)
            ->whereMonth('tanggal', date('m'))->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_buka_tutup_meter.id_pelanggan')->get();

        $data['tutup'] = BukaTutupMeterModel::where('aktif', null)
            ->whereMonth('tanggal', date('m'))->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_buka_tutup_meter.id_pelanggan')->get();
        return view('buka-tutup-meter.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sidebar'] = 'buka tutup meter';
        $data['title'] = 'buka tutup meter';
        return view('buka-tutup-meter.tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelanggan' => 'required',
            'no_ds' => 'required',
            'permintaan' => 'required',
            'tanggal' => 'required',
        ]);

        BukaTutupMeterModel::create($request->all());
        return redirect('buka-tutup-meter')->with('message', 'Buka Meter Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BukaTutupMeterModel  $bukaTutupMeterModel
     * @return \Illuminate\Http\Response
     */
    public function show(BukaTutupMeterModel $bukaTutupMeterModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BukaTutupMeterModel  $bukaTutupMeterModel
     * @return \Illuminate\Http\Response
     */
    public function edit(BukaTutupMeterModel $bukaTutupMeterModel, $id)
    {
        $data['sidebar'] = 'buka tutup meter';
        $data['title'] = 'buka tutup meter';
        $data['buktup'] = BukaTutupMeterModel::where('id_buktup_meter', $id)->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_buka_tutup_meter.id_pelanggan')->first();
        return view('buka-tutup-meter.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BukaTutupMeterModel  $bukaTutupMeterModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BukaTutupMeterModel $bukaTutupMeterModel, $id)
    {
        $request->validate([
            'no_ds' => 'required',
            'permintaan' => 'required',
            'tanggal' => 'required',
        ]);

        BukaTutupMeterModel::where('id_buktup_meter', $id)->update(
            [
                'no_ds' => $request->no_ds,
                'permintaan' => $request->permintaan,
                'tanggal' => $request->tanggal,
                'aktif' => $request->aktif,
            ]
        );
        return redirect('buka-tutup-meter')->with('message', 'Buka Tutup Meter Berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BukaTutupMeterModel  $bukaTutupMeterModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(BukaTutupMeterModel $bukaTutupMeterModel, $id)
    {
        BukaTutupMeterModel::where('id_buktup_meter', $id)->delete();
        return redirect('buka-tutup-meter')->with('message', 'Buka Tutup Meter Berhasil dihapus');
    }

    public function BukaMeterPrint()
    {
        $data['buka'] = BukaTutupMeterModel::where('aktif', 1)
            ->whereMonth('tanggal', date('m'))->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_buka_tutup_meter.id_pelanggan')->get();
        return view('buka-tutup-meter.buka-meter-print', $data);
    }

    public function TutupMeterPrint()
    {
        $data['tutup'] = BukaTutupMeterModel::where('aktif', null)
            ->whereMonth('tanggal', date('m'))->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_buka_tutup_meter.id_pelanggan')->get();
        return view('buka-tutup-meter.tutup-meter-print', $data);
    }
}
