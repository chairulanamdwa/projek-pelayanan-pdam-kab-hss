<?php

namespace App\Http\Controllers;

use App\PelangganModel;
use Illuminate\Http\Request;

class AutoKomplitController extends Controller
{
    public function pelanggan(Request $request)
    {
        $pelanggan = PelangganModel::where('nik', $request->nik)->first();

        return json_encode($pelanggan);
    }
}
