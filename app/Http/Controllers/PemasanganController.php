<?php

namespace App\Http\Controllers;

use App\PemasanganModel;
use Illuminate\Http\Request;

class PemasanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'pemasangan';
        return view('pemasangan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PemasanganModel  $pemasanganModel
     * @return \Illuminate\Http\Response
     */
    public function show(PemasanganModel $pemasanganModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PemasanganModel  $pemasanganModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PemasanganModel $pemasanganModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PemasanganModel  $pemasanganModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PemasanganModel $pemasanganModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PemasanganModel  $pemasanganModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(PemasanganModel $pemasanganModel)
    {
        //
    }
}
