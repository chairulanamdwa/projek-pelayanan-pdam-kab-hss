<?php

namespace App\Http\Controllers;

use App\Keluhan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KeluhanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['title'] = 'keluhan';
        $data['sidebar'] = 'keluhan';
        if (Auth::user()->role != 'teknisi') {
            $data['proses'] = Keluhan::where('tb_keluhan.status_perbaikan', 'proses')
                ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
                ->get();
            $data['selesai'] = Keluhan::where('tb_keluhan.status_perbaikan', 'selesai')
                ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
                ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
                ->get();
        } else {
            $data['proses'] = Keluhan::where('tb_keluhan.status_perbaikan', 'proses')
                ->where('tb_keluhan.id_teknisi', Auth::user()->id)
                ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
                ->get();
            $data['selesai'] = Keluhan::where('tb_keluhan.status_perbaikan', 'selesai')
                ->where('tb_keluhan.id_teknisi', Auth::user()->id)
                ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
                ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
                ->get();
        }

        return view('keluhan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sidebar'] = 'keluhan';
        $data['title'] = 'keluhan';
        return view('keluhan.tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelanggan' => 'required',
        ]);

        Keluhan::create([
            'id_pelanggan' => $request->id_pelanggan,
            'hp' => $request->hp,
            'jenis_keluhan' => $request->jenis_keluhan,
            'keluhan' => $request->keluhan,
            'tgl_pemeriksaan_perbaikan' => $request->tgl_pemeriksaan_perbaikan,
            'hasil_pemeriksaan' => $request->hasil_pemeriksaan,
        ]);

        return redirect()->back()->with('message', 'Keluhan Berhasil di Kirim');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Keluhan  $keluhan
     * @return \Illuminate\Http\Response
     */
    public function show(Keluhan $keluhan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Keluhan  $keluhan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['sidebar'] = 'keluhan';
        $data['title'] = 'keluhan';
        $data['teknisi'] = User::where('role', 'teknisi')->get();
        $data['keluhan'] = Keluhan::where('id_keluhan', $id)
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->first();
        return view('keluhan.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Keluhan  $keluhan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_teknisi' => 'required'
        ]);

        Keluhan::where('id_keluhan', $id)->update([
            'id_teknisi' => $request->id_teknisi,
            'biaya' => $request->biaya,
            'model' => $request->model
        ]);

        return redirect('keluhan')->with('message', 'Teknisi Berhasil di pilih');
    }

    public function ubahStatus(Request $request, $id)
    {

        Keluhan::where('id_keluhan', $id)->update([
            'status_perbaikan' => 'selesai',
            'tgl_pemeriksaan_perbaikan' => $request->tgl_pemeriksaan_perbaikan,
            'hasil_pemeriksaan' => $request->hasil_pemeriksaan,
            'tanggal_selesai' => date('Y-m-d'),
        ]);

        return redirect('/keluhan')->with('message', 'Keluhan sudah di tangani');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Keluhan  $keluhan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keluhan $keluhan)
    {
        //
    }

    public function teknisi($id)
    {
        $data['title'] = 'Teknisi';
        $data['sidebar'] = 'Teknisi';
        $data['keluhan'] = Keluhan::where('id_keluhan', $id)
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->first();

        return view('keluhan.teknisi.index', $data);
    }

    public function ganti_pipa()
    {
        $data['title'] = 'Ganti Pipa';
        $data['sidebar'] = 'Ganti Pipa';
        $data['ganti_pipa'] = Keluhan::where('tb_keluhan.status_perbaikan', 'selesai')
            ->where('jenis_keluhan', 'Ganti Pipa')
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
            ->get();
        return view('keluhan.ganti-pipa.index', $data);
    }
    public function ganti_pipa_print()
    {
        $data['title'] = 'Ganti Pipa Print';
        $data['sidebar'] = 'Ganti Pipa Print';
        $data['ganti_pipa'] = Keluhan::where('tb_keluhan.status_perbaikan', 'selesai')
            ->where('jenis_keluhan', 'Ganti Pipa')
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
            ->get();
        return view('keluhan.ganti-pipa.print', $data);
    }

    public function kinerjaTeknisi()
    {
        $data['title'] = 'Teknisi';
        $data['sidebar'] = 'Teknisi';
        $data['kinerja'] = Keluhan::where('tb_keluhan.status_perbaikan', 'selesai')
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
            ->get();
        return view('keluhan.kinerja-teknisi.index', $data);
    }

    public function kinerjaTeknisiPrint()
    {
        $data['title'] = 'Teknisi';
        $data['sidebar'] = 'Teknisi';
        $data['kinerja'] = Keluhan::where('tb_keluhan.status_perbaikan', 'selesai')
            ->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_keluhan.id_pelanggan')
            ->join('users', 'users.id', '=', 'tb_keluhan.id_teknisi')
            ->get();
        return view('keluhan.kinerja-teknisi.print', $data);
    }
}
