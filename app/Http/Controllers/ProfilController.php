<?php

namespace App\Http\Controllers;

use App\User;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProfilController extends Controller
{



    public function akun_user()
    {
        $data['sidebar'] = 'data master';
        $data['title'] = 'akun user';
        $data['user'] = User::all();
        return view('user.akun-user', $data);
    }

    public function user_create()
    {
        $data['sidebar'] = 'akun user';
        $data['title'] = 'akun user';
        return view('user.akun-user-tambah', $data);
    }

    public function user_store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'role' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:4'],
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' =>  $request->role,
            'gambar' => 'default.png',
        ]);

        return redirect('akun-user')->with('message', 'Akun user berhasil dibuat');
    }

    public function user_edit($id)
    {
        $data['sidebar'] = 'akun user';
        $data['title'] = 'akun user';
        $data['user'] = User::where('id', $id)->first();
        return view('user.akun-user-edit', $data);
    }

    public function user_update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'role' => 'required',
        ]);

        User::where('id', $id)->update([
            'name' => $request->name,
            'role' =>  $request->role,
        ]);

        return redirect('akun-user')->with('message', 'Akun user berhasil diperbarui');
    }

    public function user_destroy($id)
    {
        User::where('id', $id)->delete();
        return redirect('akun-user')->with('message', 'Akun user berhasil dihapus');
    }


    public function ganti_profil()
    {
        $data['sidebar'] = 'ganti profil';
        $data['title'] = 'akun user';
        return view('user.profil-edit', $data);
    }

    public function update_profil(Request $request)
    {
        $id = Auth::user()->id;

        $user = User::where('id', $id)->first();

        if ($request->file('gambar')) {
            if ($user->gambar != 'default.png') {
                File::delete("public/assets/img/user/" . $user->gambar);
            }
            $file = $request->file('gambar');
            $ekstensi_file1 = $file->getClientOriginalExtension();
            $nama_foto = rand() . '.' . $ekstensi_file1;
            $file->move(public_path('assets/img/user/'), $nama_foto);
        } else {
            $nama_foto = $user->gambar;
        }

        User::where('id', $id)->update([
            'name' => $request->name,
            'gambar' => $nama_foto,
        ]);


        return redirect('ganti-profil')->with('message', 'Profil berhasil di perbarui');
    }

    public function ganti_password()
    {
        $data['sidebar'] = 'ganti password';
        $data['title'] = 'akun user';
        return view('user.ganti-password', $data);
    }

    public function update_password(Request $request)
    {
        $request->validate([
            'old_password' => ['required', 'string', 'max:255'],
            'new_password' => ['required', 'string', 'min:4'],
            'konfirmasi' => ['required', 'string'],
        ]);


        if (!(Hash::check($request->old_password, Auth::user()->password))) {
            return redirect()->back()->with("message", "Password lama tidak sesuai. Harap coba lagi.");
        }

        if (strcmp($request->old_password, $request->new_password) == 0) {
            return redirect()->back()->with("message", "Password baru tidak boleh sama dengan password saat ini. Pilih password yang berbeda");
        }

        if (!(strcmp($request->new_password, $request->konfirmasi)) == 0) {
            return redirect()->back()->with("message", "Konfirmasi Password tidak sama. Ketikkan ulang Password baru.");
        }

        User::where('id', Auth::user()->id)->update([
            'password' => Hash::make($request->new_password)
        ]);

        return redirect()->back()->with("message", "Password berhasil di ganti!");
    }
}
