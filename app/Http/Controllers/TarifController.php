<?php

namespace App\Http\Controllers;

use App\TarifModel;
use Illuminate\Http\Request;

class TarifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'data master';
        $data['title'] = 'tarif';
        $data['tarif'] = TarifModel::all();
        return view('tarif.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sidebar'] = 'data master';
        $data['title'] = 'tarif';
        $data['tarif'] = TarifModel::all();
        return view('tarif.tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'deskripsi' => 'required',
            'kode' => 'required',
            'tarip_m3' => 'required',
            'minimum' => 'required',
            'abonemen' => 'required',
            'denda' => 'required',
        ]);

        TarifModel::create($request->all());

        return redirect('tarif')->with('message', 'Tarif baru berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TarifModel  $tarifModel
     * @return \Illuminate\Http\Response
     */
    public function show(TarifModel $tarifModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TarifModel  $tarifModel
     * @return \Illuminate\Http\Response
     */
    public function edit(TarifModel $tarifModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TarifModel  $tarifModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TarifModel $tarifModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TarifModel  $tarifModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(TarifModel $tarifModel, $id)
    {
        TarifModel::where('id_tarif', $id)->delete();
        return redirect('tarif')->with('message', 'Tarif baru berhasil dibuat');
    }
}
