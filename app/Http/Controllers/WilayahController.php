<?php

namespace App\Http\Controllers;

use App\Wilayah;
use Illuminate\Http\Request;

class WilayahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'Wilayah';
        $data['title'] = 'Wilayah';
        $data['wilayah'] = Wilayah::all();
        return view('wilayah.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sidebar'] = 'Wilayah';
        $data['title'] = 'Wilayah';

        return view('wilayah.tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_wilayah' => 'required'
        ]);

        Wilayah::create($request->all());

        return redirect()->back()->with('message', 'Wilayah berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wilayah  $wilayah
     * @return \Illuminate\Http\Response
     */
    public function show(Wilayah $wilayah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wilayah  $wilayah
     * @return \Illuminate\Http\Response
     */
    public function edit(Wilayah $wilayah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wilayah  $wilayah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wilayah $wilayah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wilayah  $wilayah
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Wilayah::where('id_wilayah', $id)->delete();
        return redirect()->back()->with('message', 'Wilayah telah di hapuskan');
    }
}
