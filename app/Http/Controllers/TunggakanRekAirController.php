<?php

namespace App\Http\Controllers;

use App\TunggakanRekAirModel;
use Illuminate\Http\Request;

class TunggakanRekAirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'tunggakan rek air';
        $data['title'] = 'tunggakan rek air';
        $data['tunggakan'] = TunggakanRekAirModel::whereMonth('tanggal', date('m'))->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_tunggakan_rek_air.id_pelanggan')->get();
        return view('tunggakan-rek-air.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sidebar'] = 'tunggakan rek air';
        $data['title'] = 'tunggakan rek air';
        return view('tunggakan-rek-air.tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelanggan' => 'required',
            'no_ds' => 'required',
            'tunggakan' => 'required',
            'tanggal' => 'required',
        ]);

        TunggakanRekAirModel::create($request->all());

        return redirect('tunggakan-rek-air')->with('message', 'Laporan Tunggakan Rekening Air berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TunggakanRekAirModel  $tunggakanRekAirModel
     * @return \Illuminate\Http\Response
     */
    public function show(TunggakanRekAirModel $tunggakanRekAirModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TunggakanRekAirModel  $tunggakanRekAirModel
     * @return \Illuminate\Http\Response
     */
    public function edit(TunggakanRekAirModel $tunggakanRekAirModel, $id)
    {
        $data['sidebar'] = 'tunggakan rek air';
        $data['title'] = 'tunggakan rek air';
        $data['tunggakan'] = TunggakanRekAirModel::where('id_tunggakan', $id)->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_tunggakan_rek_air.id_pelanggan')->first();
        return view('tunggakan-rek-air.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TunggakanRekAirModel  $tunggakanRekAirModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TunggakanRekAirModel $tunggakanRekAirModel, $id)
    {
        $request->validate([
            'no_ds' => 'required',
            'tunggakan' => 'required',
            'tanggal' => 'required',
        ]);

        TunggakanRekAirModel::where('id_tunggakan', $id)->update([
            'no_ds' => $request->no_ds,
            'tunggakan' => $request->tunggakan,
            'tanggal' => $request->tanggal,
        ]);

        return redirect('tunggakan-rek-air')->with('message', 'Laporan Tunggakan Rekening Air berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TunggakanRekAirModel  $tunggakanRekAirModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(TunggakanRekAirModel $tunggakanRekAirModel, $id)
    {
        TunggakanRekAirModel::where('id_tunggakan', $id)->delete();
        return redirect('tunggakan-rek-air')->with('message', 'Laporan Tunggakan Rekening Air berhasil ditambahkan');
    }

    public function print()
    {
        $data['tunggakan'] = TunggakanRekAirModel::whereMonth('tanggal', date('m'))->join('tb_pelanggan', 'tb_pelanggan.id_pelanggan', '=', 'tb_tunggakan_rek_air.id_pelanggan')->get();
        return view('tunggakan-rek-air.print', $data);
    }
}
