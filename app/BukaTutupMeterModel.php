<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukaTutupMeterModel extends Model
{
    protected $table = 'tb_buka_tutup_meter';
    protected $fillable = ['id_pelanggan', 'no_ds', 'permintaan', 'tanggal', 'aktif'];
}
