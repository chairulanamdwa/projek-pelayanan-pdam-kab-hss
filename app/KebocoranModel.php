<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KebocoranModel extends Model
{
    protected $table = 'tb_kebocoran';
    protected $fillable = ['id_pelanggan', 'no_ds', 'tgl_kebocoran', 'tgl_perbaikan', 'tgl_selesai'];
}
