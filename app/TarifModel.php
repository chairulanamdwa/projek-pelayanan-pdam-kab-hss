<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TarifModel extends Model
{
    protected $table = 'tb_tarif';
    protected $fillable = ['deskripsi', 'kode', 'tarip_m3', 'minimum', 'abonemen', 'denda'];
}
