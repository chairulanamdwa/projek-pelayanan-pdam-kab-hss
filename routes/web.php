<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => ['auth', 'admin']], function () {

    // USER
    Route::get('/akun-user', 'ProfilController@akun_user')->name('users');
    Route::get('/akun-user-tambah', 'ProfilController@user_create');
    Route::post('/akun-user', 'ProfilController@user_store');
    Route::get('/akun-user-edit/{akun_user}', 'ProfilController@user_edit');
    Route::patch('/akun-user/{akun_user}', 'ProfilController@user_update');
    Route::delete('/akun-user/{akun_user}', 'ProfilController@user_destroy');


    // GANTI METER
    Route::get('/ganti-meter', 'GantiMeterController@index')->name('gantiMeter');
    Route::get('/ganti-meter-tambah', 'GantiMeterController@create');
    Route::post('/ganti-meter', 'GantiMeterController@store');
    Route::get('/ganti-meter-edit/{ganti_meter}', 'GantiMeterController@edit');
    Route::patch('/ganti-meter/{ganti_meter}', 'GantiMeterController@update');
    Route::delete('/ganti-meter/{ganti_meter}', 'GantiMeterController@destroy');
    Route::get('/ganti-meter-print', 'GantiMeterController@print')->name('gantiMeterPrint');

    // TERA METER
    Route::get('/tera-meter', 'TeraController@index')->name('teraMeter');
    Route::get('/tera-meter-tambah', 'TeraController@create');
    Route::post('/tera-meter', 'TeraController@store');
    Route::get('/tera-meter-edit/{tera_meter}', 'TeraController@edit');
    Route::patch('/tera-meter/{tera_meter}', 'TeraController@update');
    Route::delete('/tera-meter/{tera_meter}', 'TeraController@destroy');
    Route::get('/tera-meter-print', 'TeraController@print')->name('teraMeterPrint');


    // LAPORAN KEBOCORAN
    Route::get('/kebocoran', 'KebocoranController@index')->name('kebocoran');
    Route::get('/kebocoran-tambah', 'KebocoranController@create');
    Route::post('/kebocoran', 'KebocoranController@store');
    Route::get('/kebocoran-edit/{kebocoran}', 'KebocoranController@edit');
    Route::patch('/kebocoran/{kebocoran}', 'KebocoranController@update');
    Route::delete('/kebocoran/{kebocoran}', 'KebocoranController@destroy');
    Route::get('/kebocoran-print', 'KebocoranController@print');


    // BUKA TUTUP METER
    Route::get('/buka-tutup-meter', 'BukaTutupMeterController@index')->name('bukaTutupMeter');
    Route::get('/buka-tutup-meter-tambah', 'BukaTutupMeterController@create');
    Route::post('/buka-tutup-meter', 'BukaTutupMeterController@store');
    Route::get('/buka-tutup-meter-edit/{buka_tutup_meter}', 'BukaTutupMeterController@edit');
    Route::patch('/buka-tutup-meter/{buka_tutup_meter}', 'BukaTutupMeterController@update');
    Route::delete('/buka-tutup-meter/{buka_tutup_meter}', 'BukaTutupMeterController@destroy');
    Route::get('/buka-meter-print', 'BukaTutupMeterController@BukaMeterPrint')->name('BukaMeterPrint');
    Route::get('/tutup-meter-print', 'BukaTutupMeterController@TutupMeterPrint')->name('TutupMeterPrint');


    // TUNGGAKAN REK AIR
    Route::get('/tunggakan-rek-air', 'TunggakanRekAirController@index')->name('tunggakanRekAir');
    Route::get('/tunggakan-rek-air-tambah', 'TunggakanRekAirController@create');
    Route::post('/tunggakan-rek-air', 'TunggakanRekAirController@store');
    Route::get('/tunggakan-rek-air-edit/{tunggakan_rek_air}', 'TunggakanRekAirController@edit');
    Route::patch('/tunggakan-rek-air/{tunggakan_rek_air}', 'TunggakanRekAirController@update');
    Route::delete('/tunggakan-rek-air/{tunggakan_rek_air}', 'TunggakanRekAirController@destroy');
    Route::get('/tunggakan-rek-air-print', 'TunggakanRekAirController@print');


    // PEMASANGANAN SAMBUNG BARU
    Route::get('/pemasangan', 'PemasanganController@index')->name('pemasangan');
    Route::get('/pemasangan-tambah', 'PemasanganController@create');
    Route::post('/pemasangan', 'PemasanganController@store');
    Route::get('/pemasangan-edit/{pemasangan}', 'PemasanganController@edit');
    Route::patch('/pemasangan/{pemasangan}', 'PemasanganController@update');
    Route::delete('/pemasangan/{pemasangan}', 'PemasanganController@destroy');
    Route::get('/pemasangan-print', 'PemasanganController@print');

    // PELANGGAN
    Route::get('/pelanggan', 'PelangganController@index')->name('pelanggan');
    Route::get('/pelanggan-tambah', 'PelangganController@create');
    Route::post('/pelanggan', 'PelangganController@store');
    Route::get('/pelanggan-edit/{pelanggan}', 'PelangganController@edit');
    Route::patch('/pelanggan/{pelanggan}', 'PelangganController@update');
    Route::delete('/pelanggan/{pelanggan}', 'PelangganController@destroy');
    Route::get('/pelanggan-print', 'PelangganController@print');


    // GOLONGAN TARIF
    Route::get('/tarif', 'TarifController@index')->name('tarif');
    Route::get('/tarif-tambah', 'TarifController@create');
    Route::post('/tarif', 'TarifController@store');
    Route::delete('/tarif/{tarif}', 'TarifController@destroy');

    // KELUHAN

    Route::get('/keluhan-update/{keluhan}', 'KeluhanController@edit')->name('keluhanUpdate');
    Route::patch('/keluhan-update/{keluhan}', 'KeluhanController@update')->name('keluhanPatch');

    // LAPORAN GANTI PIPA
    Route::get('/ganti-pipa', 'KeluhanController@ganti_pipa')->name('gantiPipa');
    Route::get('/ganti-pipa-print', 'KeluhanController@ganti_pipa_print')->name('gantiPipaPrint');

    // KINERJA TEKNISI
    Route::get('/kinerja-teknisi', 'KeluhanController@kinerjaTeknisi')->name('kinerjaTeknisi');
    Route::get('/kinerja-teknisi-print', 'KeluhanController@kinerjaTeknisiPrint')->name('kinerjaTeknisiPrint');

    Route::get('/wilayah', 'WilayahController@index')->name('wilayah');
    Route::get('/wilayah-tambah', 'WilayahController@create');
    Route::post('/wilayah', 'WilayahController@store');
    Route::get('/wilayah-edit/{wilayah}', 'WilayahController@edit');
    Route::patch('/wilayah/{wilayah}', 'WilayahController@update');
    Route::delete('/wilayah/{wilayah}', 'WilayahController@destroy');
    Route::get('/wilayah-print', 'WilayahController@print');
});


Route::group(['middleware' => ['auth', 'teknisi']], function () {

    // KELUHAN
    Route::get('/keluhan', 'KeluhanController@index')->name('keluhan');
    Route::patch('/keluhan-ubah-status/{keluhan}', 'KeluhanController@ubahStatus')->name('ubahStatusKeluhan');
    Route::get('/teknisi/{keluhan}', 'KeluhanController@teknisi')->name('teknisi');
});



Route::group(['middleware' => ['auth']], function () {

    // PAGES
    Route::get('/', 'PagesController@dashboard')->name('dashboard');
    Route::get('/menu-layanan', 'PagesController@menuLayanan')->name('menuLayanan');
    Route::get('/autokomplit-pelanggan', 'AutoKomplitController@pelanggan');


    Route::get('/ganti-profil', 'ProfilController@ganti_profil')->name('gantiProfil');
    Route::patch('/ganti-profil', 'ProfilController@update_profil');
    Route::get('/ganti-password', 'ProfilController@ganti_password')->name('gantiPassword');
    Route::patch('/ganti-password', 'ProfilController@update_password');


    // KELUHAN
    Route::get('/keluhan-add', 'KeluhanController@create')->name('keluhanAdd');
    Route::post('/keluhan-store', 'KeluhanController@store')->name('keluhanStore');
});
