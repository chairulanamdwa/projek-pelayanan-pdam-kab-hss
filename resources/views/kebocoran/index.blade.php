@extends('templates.main')
@section('title')

Kebocoran

@endsection
@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a class="badge mb-3" href="{{route('menuLayanan')}}"><i class="fas fa-arrow-left"></i> Kembali</a>
<a href="{{url('kebocoran-tambah')}}" class="btn btn-sm btn-primary">
    <i class=" fas fa-plus"></i>
    Kebocoran
</a>
<div class="row">
    <div class="col-lg-12">
        <div class="card border-0 shadow">
            <div class="card-body">
                <table class="table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0">
                    <thead style="">
                        <tr>
                            <th>#</th>
                            <th><span class="text-danger">*</span></th>
                            <th>No DS</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Tanggal Kebocoran</th>
                            <th>Tanggal Perbaikan</th>
                            <th>Tanggal Selesai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kebocoran as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td class="text-center">
                                <form action="{{url('kebocoran').'/'.$item->id_kebocoran}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-xs text-danger" style="background:transparent"><i
                                            class="fa fa-times"></i></button>
                                </form>
                                <a href="{{url('kebocoran-edit').'/'.$item->id_kebocoran}}" class=""><i
                                        class="fa fa-edit text-success"></i></a>
                            </td>
                            <td>{{$item->no_ds}}</td>
                            <td>{{$item->nama_pelanggan}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{date('d/m/Y',strtotime($item->tgl_kebocoran))}}</td>
                            <td>{{date('d/m/Y',strtotime($item->tgl_perbaikan))}}</td>
                            <td>{{date('d/m/Y',strtotime($item->tgl_selesai))}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
                <a href="{{url('kebocoran-print')}}" class="badge" target="_blank">
                    <i class="fas fa-print"></i> Cetak Laporan
                </a>
            </div>
        </div>

    </div>
</div>

@endsection