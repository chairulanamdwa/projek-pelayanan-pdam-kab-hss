@extends('templates.main')
@section('title','Edit Data Laporan Kebocoran')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a href="{{url('kebocoran')}}" class="badge" style="margin-right:20px"><i class="fa fa-arrow-left"></i> Kembali</a>
<br><br>

<form action="{{url('kebocoran').'/'.$kebocoran->id_kebocoran}}" method="POST">
    @method('patch')
    @csrf
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card shadow border-0">
                <div class="card-body">
                    <h3><span id="nama-pelanggan">{{$kebocoran->nama_pelanggan}}</span></h3>
                    <div class="form-group">
                        <label for="no_ds">Nomor DS</label>
                        <input type="text" name="no_ds" id="no_ds" class="form-control" value="{{$kebocoran->no_ds}}">
                        @if ($errors->has('no_ds'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('no_ds') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tgl_kebocoran">Tanggal Kebocoran</label>
                        <input type="date" name="tgl_kebocoran" id="tgl_kebocoran" class="form-control"
                            value="{{date('Y-m-d',strtotime($kebocoran->tgl_kebocoran))}}">
                        @if ($errors->has('tgl_kebocoran'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_kebocoran') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tgl_perbaikan">Tanggal Perbaikan</label>
                        <input type="date" name="tgl_perbaikan" id="tgl_perbaikan" class="form-control"
                            value="{{date('Y-m-d',strtotime($kebocoran->tgl_perbaikan))}}">
                        @if ($errors->has('tgl_perbaikan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_perbaikan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tgl_selesai">Tanggal Selesai</label>
                        <input type="date" name="tgl_selesai" id="tgl_selesai" class="form-control"
                            value="{{date('Y-m-d',strtotime($kebocoran->tgl_selesai))}}">
                        @if ($errors->has('tgl_selesai'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_selesai') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i> Ubah Laporan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    $('#nik').on('input',function(){
            const nik = $('#nik').val();
            $('#nama-pelanggan').html('');
            $.ajax({
                url:"{{url('autokomplit-pelanggan')}}",
                type:'get',
                data:{nik:nik},
                success:function(data){
                    const json = JSON.parse(data);

                    if(json){
                        $('#nama-pelanggan').html(json.nama_pelanggan);
                        $('#id_pelanggan').val(json.id_pelanggan);
                    }else{
                        $('#nama-pelanggan').html("<span class='text-danger'>NIK tidak terdaftar<span>");
                    }
                }
            });
            
        });
</script>
@endsection