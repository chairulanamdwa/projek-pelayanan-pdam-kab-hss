@extends('templates.print')
@section('title')
Kebocoran
@endsection

@section('content')
<table class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead style="">
        <tr>
            <th>#</th>
            <th>No DS</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Tanggal Kebocoran</th>
            <th>Tanggal Perbaikan</th>
            <th>Tanggal Selesai</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($kebocoran as $item)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$item->no_ds}}</td>
            <td>{{$item->nama_pelanggan}}</td>
            <td>{{$item->alamat}}</td>
            <td>{{date('d/m/Y',strtotime($item->tgl_kebocoran))}}</td>
            <td>{{date('d/m/Y',strtotime($item->tgl_perbaikan))}}</td>
            <td>{{date('d/m/Y',strtotime($item->tgl_selesai))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection