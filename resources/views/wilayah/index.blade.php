@extends('templates.main')
@section('title')
Wilayah
@endsection
@section('content')


@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif


<a href="{{url('wilayah-tambah')}}" class="btn btn-sm btn-primary">
    Tambah Wilayah
</a>

<br><br>

<div class="row">
    @foreach ($wilayah as $item)
    <div class="col-6 col-md-3 mt-3">
        <div class="list-group  border-0">
            <a href="{{route('bukaTutupMeter')}}" class="list-group-item list-group-item-action border-0 shadow ">
                <i class="fas fa-map-marker-alt fa-fw mr-2"></i>
                {{$item->nama_wilayah}}
                <form action="{{url('wilayah').'/'.$item->id_wilayah}}" class="d-inline " method="POST">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-transparent">
                        <i class="fas fa-times text-danger"></i>
                    </button>
                </form>
            </a>
        </div>
    </div>
    @endforeach
</div>


@endsection