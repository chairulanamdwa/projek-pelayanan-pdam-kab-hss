@extends('templates.main')
@section('title')
Wilayah
@endsection
@section('content')



@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif


<a href="{{url('wilayah')}}" class="badge">
    <i class="fas fa-arrow-left"></i> kembari
</a>

<br><br>

<div class="row">
    <div class="col-12 col-md-6">
        <div class="card shadow border-0">
            <div class="card-body">
                <form action="{{url('wilayah')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nama_wilayah">Nama Wilayah</label>
                        <input type="text" name="nama_wilayah" id="nama_wilayah" class="form-control" autofocus>
                        @if ($errors->has('nama_wilayah'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama_wilayah') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Tambah Wilayah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection