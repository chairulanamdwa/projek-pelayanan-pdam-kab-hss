@extends('templates.main')
@section('title')
Ganti Meter
@endsection
@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a class="badge mb-3" href="{{route('menuLayanan')}}"><i class="fas fa-arrow-left"></i> Kembali</a>
<div class="row" style="background:white;padding:20px;border-radius:5px;">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0">
                <thead style="">
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Tanggal Permintaan</th>
                        <th>Nama Teknisi</th>
                        <th>Tanggal Perbaikan</th>
                        <th>Biaya Perbaikan</th>
                    </tr>
                </thead>
                <tbody id="tabeldata">
                    @foreach ($gantiMeter as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$item->nama_pelanggan}}</td>
                        <td>{{$item->alamat}}</td>
                        <td>{{date('d/m/Y',strtotime($item->created_at))}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{date('d/m/Y',strtotime($item->tgl_pemeriksaan_perbaikan))}}</td>
                        <td>Rp.{{number_format($item->biaya)}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <br>
        <a href="{{route('gantiMeterPrint')}}" class="badge" target="_blank">
            <i class="fas fa-print"></i> Print Laporan
        </a>
    </div>
</div>

@endsection