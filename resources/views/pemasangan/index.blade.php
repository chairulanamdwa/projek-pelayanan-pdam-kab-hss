@extends('templates.main')
@section('title')
@php
$bulan = [
'Januari',
'Februari',
'Maret',
'April',
'Mei',
'Juni',
'Juli',
'Agustus',
'September',
'Oktober',
'November',
'Desember'
];
@endphp

Pemasangan Bulan {{$bulan[date('m')-1]}}
@endsection
@section('title2','Tabel Tunggakan Rek Air')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a class="badge mb-3" href="{{route('menuLayanan')}}"><i class="fas fa-arrow-left"></i> Kembali</a>
<div class="row" style="background:white;padding:20px;border-radius:5px;">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover " id="dataTable" width="100%" cellspacing="0">
                <thead style="">
                    <tr>
                        <th>#</th>
                        <th><span class="text-danger">*</span></th>
                        <th>No DS</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Tunggakan</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- @foreach ($pemasangan as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                    <td class="text-center">
                        <form action="{{url('tunggakan-rek-air').'/'.$item->id_tunggakan}}" method="POST">
                            @method('delete')
                            @csrf
                            <button class="btn btn-xs text-danger" style="background:transparent"><i
                                    class="fa fa-times"></i></button>
                        </form>
                        <a href="{{url('tunggakan-rek-air-edit').'/'.$item->id_tunggakan}}" class="text-success"><i
                                class="fa fa-edit"></i></a>
                    </td>
                    <td>{{$item->no_ds}}</td>
                    <td>{{$item->nama_pelanggan}}</td>
                    <td>{{$item->alamat}}</td>
                    <td>{{$item->tunggakan}}</td>
                    </tr>
                    @endforeach --}}
                </tbody>
            </table>
        </div>
        <a href="{{url('pemasangan-tambah')}}" class=" btn btn-xs" style="background:#ddd;color:black;font-weight:bold">
            <img src="{{url('public/assets/img/sidebar-icon/news.png')}}" width="20px" height="20px"> Tambah Tunggakan
            Rek Air
        </a>
        <a href="{{url('tunggakan-rek-air-print')}}" class=" btn btn-xs" target="_blank"
            style="background:#ddd;color:black;font-weight:bold">
            <img src="{{url('public/assets/img/ios-icon/pages.png')}}" width="20px" height="20px"> Cetak Laporan
        </a>
    </div>
</div>

@endsection