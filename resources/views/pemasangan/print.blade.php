@extends('templates.print')
@section('title')
@php
    $bulan = [
1 =>    'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
];
@endphp

Tunggakan Rekening Air {{$bulan[date('m')]}}
@endsection

@section('content')
    
        <table class="table table-striped table-bordered" >
            <thead style="">
                <tr>
                    <th>#</th>
                    <th>No DS</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Tunggakan</th>
                    <th>Tanggal</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tunggakan as $item)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->no_ds}}</td>
                    <td>{{$item->nama_pelanggan}}</td>
                    <td>{{$item->alamat}}</td>
                    <td>{{$item->tunggakan}}</td>
                    <td>{{date('d/m/Y',strtotime($item->tanggal))}}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
@endsection