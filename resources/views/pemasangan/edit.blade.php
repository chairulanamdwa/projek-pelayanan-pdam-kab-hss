@extends('templates.main')
@section('title1','Laporan Tunggakan Rekening Air')
@section('title2','Edit Laporan Tunggakan Rekening Air')

@section('content')
       
@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif 

<a href="{{url('tunggakan-rek-air')}}" class="text-muted" style="margin-right:20px"><i class="fa fa-arrow-left"></i> Kembali</a>
<br><br>

<form action="{{url('tunggakan-rek-air').'/'.$tunggakan->id_tunggakan}}" method="POST">
@method('patch')
@csrf

<h3><span id="nama-pelanggan">{{$tunggakan->nama_pelanggan}}</span></h3>
<div class="row" >
    <div class="col-xs-6">
        <div class="form-group">
            <label for="no_ds">Nomor DS</label>
            <input type="text" name="no_ds" id="no_ds" class="form-control" value="{{$tunggakan->no_ds}}">
            @if ($errors->has('no_ds'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('no_ds') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="tunggakan">Tunggakan</label>
            <input type="text" name="tunggakan" id="tunggakan" class="form-control" value="{{$tunggakan->tunggakan}}">
            @if ($errors->has('tunggakan'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('tunggakan') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="tanggal">Tanggal</label>
            <input type="date" name="tanggal" id="tanggal" class="form-control" value="{{date('Y-m-d',strtotime($tunggakan->tanggal))}}">
            @if ($errors->has('tanggal'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('tanggal') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
                <button class="btn btn-primary"><i class="fa fa-pencil"></i> Ubah Laporan</button>
        </div>
    </div>
</div>
</form>


@endsection

@section('script')
    <script>
        $('#nik').on('input',function(){
            const nik = $('#nik').val();
            $('#nama-pelanggan').html('');
            $.ajax({
                url:"{{url('autokomplit-pelanggan')}}",
                type:'get',
                data:{nik:nik},
                success:function(data){
                    const json = JSON.parse(data);

                    if(json){
                        $('#nama-pelanggan').html(json.nama_pelanggan);
                        $('#id_pelanggan').val(json.id_pelanggan);
                    }else{
                        $('#nama-pelanggan').html("<span class='text-danger'>NIK tidak terdaftar<span>");
                    }
                }
            });
            
        });
    </script>
@endsection