@extends('templates.main')
@section('title')
Tera Meter
@endsection
@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a class="badge mb-3" href="{{route('menuLayanan')}}"><i class="fas fa-arrow-left"></i> Kembali</a>
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow border-0">
            <div class="card-body">
                <table class="table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0">
                    <thead style="">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Tanggal Permintaan</th>
                            <th>Nama Teknisi</th>
                            <th>Tanggal Pemeriksaan</th>
                            <th>Hasil Pemeriksaan</th>
                        </tr>
                    </thead>
                    <tbody id="tabeldata">
                        @foreach ($tera as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->nama_pelanggan}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{date('d/m/Y',strtotime($item->created_at))}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{date('d/m/Y',strtotime($item->tgl_pemeriksaan_perbaikan))}}</td>
                            <td>{{$item->hasil_pemeriksaan}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
                <a href="{{route('teraMeterPrint')}}" class="badge" target="_blank">
                    <i class="fas fa-print"></i> Print Laporan
                </a>
            </div>
        </div>
        </>
    </div>

    @endsection