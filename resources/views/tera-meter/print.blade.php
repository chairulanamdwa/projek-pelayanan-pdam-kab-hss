@extends('templates.print')
@section('title')
Tera Meter
@endsection

@section('content')
<table class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead style="">
        <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Tanggal Permintaan</th>
            <th>Nama Teknisi</th>
            <th>Tanggal Pemeriksaan</th>
            <th>Hasil Pemeriksaan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tera as $item)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$item->nama_pelanggan}}</td>
            <td>{{$item->alamat}}</td>
            <td>{{date('d/m/Y',strtotime($item->created_at))}}</td>
            <td>{{$item->name}}</td>
            <td>{{date('d/m/Y',strtotime($item->tgl_pemeriksaan_perbaikan))}}</td>
            <td>{{$item->hasil_pemeriksaan}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection