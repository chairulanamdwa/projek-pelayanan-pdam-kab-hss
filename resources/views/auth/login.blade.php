@extends('templates.auth')
@section('title','Login')
@section('content')
<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-white text-md-right">Alamat Email</label>
        <div class="col-md-6">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-white text-md-right">Password</label>
        <div class="col-md-6">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                name="password" required>

            @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-4">
        <input class="form-check-input" type="checkbox" name="remember" id="remember"
            {{ old('remember') ? 'checked' : '' }}>
        <label class="form-check-label text-white" for="remember">
            Ingat Saya
        </label>
    </div>
    <br>
    <br>
    <div class="form-group row mb-0">
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-block">
                {{ __('Login') }}
            </button>
        </div>
    </div>
</form>
<p class="text-white">Belum punya akun? <a href="{{url('register')}}">Daftar</a></p>
@endsection