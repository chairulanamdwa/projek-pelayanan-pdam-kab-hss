@extends('templates.main')
@section('title','List Tarif PDAM')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
<br>

<a href="{{route('pelanggan')}}" class="badge mb-4"><i class="fas fa-arrow-left fa-fw"></i> Kembali</a>
<div class="row" style="background:white;padding:20px;border-radius:5px;">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead style="">
                    <tr>
                        <th>#</th>
                        <th><span class="text-danger">*</span></th>
                        <th>Deskripsi</th>
                        <th>Kode</th>
                        <th class="text-center">Pemakaian (M3)</th>
                        <th class="text-center" style="width:20px">Tarip (Rp/M3)</th>
                        <th>Minimun Pemakaian</th>
                        <th>Abonemen</th>
                        <th>Denda</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tarif as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td class="text-center">
                            <form action="{{url('tarif').'/'.$item->id_tarif}}" method="POST">
                                @method('delete')
                                @csrf
                                <button class="btn btn-xs text-danger" style="background:transparent"><i
                                        class="fa fa-times"></i></button>
                            </form>
                            {{-- <a href="{{url('tarif-edit').'/'.$item->id_tarif}}" class="text-success"><i
                                class="fa fa-edit"></i></a> --}}
                        </td>
                        <td>{{$item->deskripsi}}</td>
                        <td>{{$item->kode}}</td>
                        <td class="text-center">
                            0 - 1 <br>
                            11 - 20 <br>
                            21 - 30 <br>
                            diatas 30 <br>
                        </td>
                        <td class="text-center">{{$item->tarip_m3}}</td>
                        <td>{{$item->minimum}}</td>
                        <td>{{$item->abonemen}}</td>
                        <td>{{$item->denda}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <a href="{{url('tarif-tambah')}}" class=" btn btn-xs" style="background:#ddd;color:black;font-weight:bold">
            <img src="{{url('public/assets/img/ios-icon/photos.png')}}" width="20px" height="20px"> Tambah Tarif
        </a>
        <a href="{{url('pelanggan')}}" class=" btn btn-xs" style="background:#ddd;color:black;font-weight:bold">
            <img src="{{url('public/assets/img/sidebar-icon/contacts.png')}}" width="20px" height="20px"> Data Pelanggan
        </a>
    </div>
</div>
@endsection