@extends('templates.main')
@section('title','Buat Tarif Baru')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a href="{{url('tarif')}}" class="badge" style="margin-right:20px"><i class="fa fa-arrow-left"></i> Kembali</a>
<br><br>

<form action="{{url('tarif')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea name="deskripsi" id="deskripsi" class="form-control"></textarea>
                        @if ($errors->has('deskripsi'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('deskripsi') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="kode">Kode</label>
                        <input type="text" name="kode" id="kode" class="form-control">
                        @if ($errors->has('kode'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('kode') }}</strong>
                        </span>
                        @endif
                    </div>
                    {{-- <div class="form-group">
                    <label for="pemakaian_m3">Pemakaian (M3)</label>
                    <textarea name="pemakaian_m3" id="pemakaian_m3" class="form-control" ></textarea>
                    @if ($errors->has('pemakaian_m3'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pemakaian_m3') }}</strong>
                    </span>
                    @endif
                </div> --}}
                <div class="form-group">
                    <label for="tarip_m3">Tarip (Rp/M3)</label>
                    <textarea type="text" name="tarip_m3" id="tarip_m3" class="form-control"></textarea>
                    @if ($errors->has('tarip_m3'))
                    <span class="text-danger" role="alert">
                        <strong>{{ $errors->first('tarip_m3') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="minimum">Minimum Pemakaian</label>
                    <input type="text" name="minimum" id="minimum" class="form-control">
                    @if ($errors->has('minimum'))
                    <span class="text-danger" role="alert">
                        <strong>{{ $errors->first('minimum') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="abonemen">Abonemen</label>
                    <input type="text" name="abonemen" id="abonemen" class="form-control">
                    @if ($errors->has('abonemen'))
                    <span class="text-danger" role="alert">
                        <strong>{{ $errors->first('abonemen') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="denda">Denda</label>
                    <input type="text" name="denda" id="denda" class="form-control">
                    @if ($errors->has('denda'))
                    <span class="text-danger" role="alert">
                        <strong>{{ $errors->first('denda') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <button class="btn btn-primary"><i class="fa fa-save fa-fw"></i> Buat Tarif</button>
                </div>
            </div>
        </div>
    </div>
    </div>
</form>


@endsection