@extends('templates.main')
@section('title')
Buat Keluhan
@endsection
@section('content')


@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

@if (Auth::user()->role != 'user')
<a class="badge mb-3" href="{{route('pelanggan')}}"><i class="fas fa-folder"></i> Ke Tabel Keluhan</a>
@endif

<br><br>
<form action="{{route('keluhanStore')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card shadow border-0">
                <div class="card-body">
                    <h3><span id="nama-pelanggan"></span></h3>
                    <div class="form-group">
                        <label for="nik">NIK Pelanggan</label>
                        <input type="text" name="nik" id="nik" class="form-control">
                        <input type="hidden" name="id_pelanggan" id="id_pelanggan" class="form-control">
                        @if ($errors->has('id_pelanggan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('id_pelanggan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="hp">Nomor Handphone (Opsional)</label>
                        <input type="number" name="hp" id="hp" class="form-control">
                        @if ($errors->has('hp'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('hp') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="jenis_keluhan">Jenis Keluhan</label>
                        <select name="jenis_keluhan" id="jenis_keluhan" class="form-control">
                            <option value="">..Pilih..</option>
                            <option value="Ganti Pipa">Ganti Pipa</option>
                            <option value="Ganti Meter">Ganti Meter</option>
                            <option value="Tera Meter">Tera Meter</option>
                            <option value="Kebocoran">Kebocoran</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                        @if ($errors->has('jenis_keluhan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('jenis_keluhan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group" id="keluhan">
                        <label for="keluhan">Keluhan Lainnya</label>
                        <textarea rows="4" name="keluhan" class="form-control"></textarea>
                        @if ($errors->has('keluhan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('keluhan') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i> Kirim Keluhan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection



@section('script')
<script>
    $('#nik').on('input',function(){
            const nik = $('#nik').val();
            $('#nama-pelanggan').html('');
            $('#id_pelanggan').val('');
            $.ajax({
                url:"{{url('autokomplit-pelanggan')}}",
                type:'get',
                data:{nik:nik},
                success:function(data){
                    const json = JSON.parse(data);

                    if(json){
                        $('#nama-pelanggan').html(json.nama_pelanggan);
                        $('#id_pelanggan').val(json.id_pelanggan);
                    }else{
                        $('#nama-pelanggan').html("<span class='text-danger'>NIK tidak terdaftar<span>");
                    }
                }
            });
            
        });

        $('#keluhan').hide();

        $('#jenis_keluhan').on('change',function(){
            $('#keluhan').hide();
            const val = $(this).val();
            if (val == 'Lainnya') {
                $('#keluhan').show();
            } else if(val == 'Lainnya'){
            }
        });


</script>
@endsection