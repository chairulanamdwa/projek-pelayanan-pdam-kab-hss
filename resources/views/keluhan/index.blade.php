@extends('templates.main')
@section('title')
Keluhan yang masuk
@endsection
@section('content')



<a class="badge mb-3" href="{{route('pelanggan')}}"><i class="fas fa-folder"></i> Ke Pelanggan</a>
<div class="row">
    <div class="col-md-6 col-lg-6 col-12">
        <div class="card shadow border-0">
            <div class="card-body">
                <h5>Keluhan yang sedang di proses</h5><br><br>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" width="100%"
                        cellspacing="0">
                        <thead style="">
                            <tr>
                                <th>#</th>
                                <th><span class="text-danger">*</span></th>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Keluhan</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th>Nomor Handphone</th>
                                <th>Tanggal Keluhan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($proses as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>

                                    @if (Auth::user()->role != 'teknisi')

                                    @if ($item->id_teknisi)
                                    <button disabled class="btn btn-sm btn-primary" de>
                                        Teknisi dipilih
                                    </button>
                                    @else
                                    <a href="{{route('keluhanUpdate',$item->id_keluhan)}}"
                                        class="btn btn-sm btn-success">
                                        Pilih Teknisi
                                    </a>
                                    @endif

                                    @else

                                    @if ($item->status_perbaikan == 'selesai')
                                    <button disabled class="btn btn-sm btn-primary" de>
                                        Selesai
                                    </button>
                                    @else
                                    <a href="{{route('teknisi',$item->id_keluhan)}}" class="btn btn-sm btn-success">
                                        Ubah Status Keluhan
                                    </a>
                                    @endif

                                    @endif


                                </td>
                                <td>{{$item->nama_pelanggan}}</td>
                                <td>{{$item->nik}}</td>
                                @if ($item->jenis_keluhan == 'Lainnya')
                                <td>{{$item->keluhan}}</td>
                                @else
                                <td>{{$item->jenis_keluhan}}</td>
                                @endif
                                @if ($item->jenis_keluhan == 'Ganti Pipa')
                                <td>
                                    <ul>
                                        <li><span class="font-weight-bold">Biaya</span> : {{$item->biaya}}</li>
                                        <li> <span class="font-weight-bold">Model Pipa</span>: {{$item->model}}</li>
                                    </ul>
                                </td>
                                @elseif ($item->jenis_keluhan == 'Ganti Meter')
                                <td>
                                    <ul>
                                        <li>
                                            <span class="font-weight-bold">Biaya Perbaikan</span> : {{$item->biaya}}
                                        </li>
                                    </ul>
                                </td>
                                @elseif($item->jenis_keluhan == 'Kebocoran')
                                <td>{{$item->jenis_keluhan}}</td>
                                @else
                                <td>{{$item->keluhan}}</td>
                                @endif
                                <td>
                                    @if ($item->status_perbaikan == 'proses')
                                    <span class="badge badge-warning">Proses</span>
                                    @else
                                    <span class="badge badge-success">Selesai</span>
                                    @endif
                                </td>
                                <td>{{$item->hp}}</td>
                                <td>{{date('d-m-Y',strtotime($item->created_at))}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if (Auth::user()->role != 'user')
    <div class="col-md-6 col-lg-6 col-12">
        <div class="card shadow border-0">
            <div class="card-body">
                <h5>Keluhan yang sudah selesai </h5><br><br>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" width=" 100%"
                        cellspacing="0">
                        <thead style="">
                            <tr>
                                <th>#</th>
                                <th><span class="text-danger">*</span></th>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Keluhan</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th>Nomor Handphone</th>
                                <th>Teknisi</th>
                                <th>Tanggal Keluhan</th>
                                <th>Tanggal Selesai</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($selesai as $item)


                            @php
                            $mulai = new DateTime($item->created_at);
                            $selesai = new DateTime($item->tanggal_selesai);
                            $interval = $mulai->diff($selesai);
                            @endphp

                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>

                                    @if (Auth::user()->role != 'teknisi')

                                    @if ($item->id_teknisi)
                                    <button disabled class="btn btn-sm btn-primary" de>
                                        Teknisi dipilih
                                    </button>
                                    @else
                                    <a href="{{route('keluhanUpdate',$item->id_keluhan)}}"
                                        class="btn btn-sm btn-success">
                                        Pilih Teknisi
                                    </a>
                                    @endif

                                    @else

                                    @if ($item->status_perbaikan == 'selesai')
                                    {{-- <button disabled class="btn btn-sm btn-primary" de>
                                        Selesai
                                    </button> --}}
                                    @else
                                    <form action="{{route('ubahStatusKeluhan',$item->id_keluhan)}}" method="POST">
                                        @method('PATCH')
                                        @csrf
                                        <button class="btn btn-sm btn-success">
                                            Ubah Status Menjadi Selesai
                                        </button>
                                    </form>
                                    @endif

                                    @endif


                                </td>
                                <td>{{$item->nama_pelanggan}}</td>
                                <td>{{$item->nik}}</td>
                                @if ($item->jenis_keluhan == 'Lainnya')
                                <td>{{$item->keluhan}}</td>
                                @else
                                <td>{{$item->jenis_keluhan}}</td>
                                @endif
                                @if ($item->jenis_keluhan == 'Ganti Pipa')
                                <td>
                                    <ul>
                                        <li><span class="font-weight-bold">Biaya</span> : {{$item->biaya}}</li>
                                        <li> <span class="font-weight-bold">Model Pipa</span>: {{$item->model}}</li>
                                    </ul>
                                </td>
                                @elseif ($item->jenis_keluhan == 'Ganti Meter')
                                <td>
                                    <ul>
                                        <li><span class="font-weight-bold">Biaya Perbaikan</span> : {{$item->biaya}}
                                        </li>
                                    </ul>
                                </td>
                                @elseif($item->jenis_keluhan == 'Kebocoran')
                                <td>{{$item->jenis_keluhan}}</td>
                                @else
                                <td>{{$item->keluhan}}</td>
                                @endif
                                <td>
                                    @if ($item->status_perbaikan == 'proses')
                                    <span class="badge badge-warning">Proses</span>
                                    @else
                                    <span class="badge badge-success">Selesai</span>
                                    @endif
                                </td>
                                <td>{{$item->hp}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{date('d-m-Y',strtotime($item->created_at))}}</td>
                                <td>
                                    <ul>
                                        <li><span class="font-weight-bold">Tanggal Selesai</span> :
                                            {{date('d-m-Y',strtotime($item->tanggal_selesai))}}</li>
                                        <li><span class="font-weight-bold">Jangka Waktu Penyelesaian</span> :
                                            {{$interval->days}} Hari</li>
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>


@endsection