@extends('templates.main')
@section('title')
Laporan Kinerja Teknisi
@endsection
@section('content')

<a class="badge mb-3" href="{{route('pelanggan')}}"><i class="fas fa-folder"></i> Ke Pelanggan</a>
<div class="row">
    <div class="col-md-12 col-lg-12 col-12">
        <div class="card shadow border-0">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" width=" 100%"
                        cellspacing="0">
                        <thead style="">
                            <tr>
                                <th>#</th>
                                <th>Nama Teknisi</th>
                                <th>Keluhan</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th>Nomor Handphone</th>
                                <th>Nama Pelanggan</th>
                                <th>Tanggal Keluhan</th>
                                <th>Tanggal Selesai</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kinerja as $item)


                            @php
                            $mulai = new DateTime($item->created_at);
                            $selesai = new DateTime($item->tanggal_selesai);
                            $interval = $mulai->diff($selesai);
                            @endphp

                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->name}}</td>
                                @if ($item->jenis_keluhan == 'Lainnya')
                                <td>{{$item->keluhan}}</td>
                                @else
                                <td>{{$item->jenis_keluhan}}</td>
                                @endif
                                @if ($item->jenis_keluhan == 'Ganti Pipa')
                                <td>
                                    <ul>
                                        <li><span class="font-weight-bold">Biaya</span> : {{$item->biaya}}</li>
                                        <li> <span class="font-weight-bold">Model Pipa</span>: {{$item->model}}</li>
                                    </ul>
                                </td>
                                @elseif ($item->jenis_keluhan == 'Ganti Meter')
                                <td>
                                    <ul>
                                        <li><span class="font-weight-bold">Biaya Perbaikan</span> : {{$item->biaya}}
                                        </li>
                                    </ul>
                                </td>
                                @elseif($item->jenis_keluhan == 'Kebocoran')
                                <td>{{$item->jenis_keluhan}}</td>
                                @else
                                <td>{{$item->keluhan}}</td>
                                @endif
                                <td>
                                    @if ($item->status_perbaikan == 'proses')
                                    <span class="badge badge-warning">Proses</span>
                                    @else
                                    <span class="badge badge-success">Selesai</span>
                                    @endif
                                </td>
                                <td>{{$item->hp}}</td>
                                <td>{{$item->nama_pelanggan}}</td>
                                <td>{{date('d-m-Y',strtotime($item->created_at))}}</td>
                                <td>
                                    <ul>
                                        <li><span class="font-weight-bold">Tanggal Selesai</span> :
                                            {{date('d-m-Y',strtotime($item->tanggal_selesai))}}</li>
                                        <li><span class="font-weight-bold">Jangka Waktu Penyelesaian</span> :
                                            {{$interval->days}} Hari</li>
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="{{route('kinerjaTeknisiPrint')}}" class="badge" target="_blank">
                        <i class="fas fa-print"></i> Print Laporan
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection