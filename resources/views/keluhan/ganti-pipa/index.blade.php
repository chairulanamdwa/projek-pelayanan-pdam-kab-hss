@extends('templates.main')
@section('title')
Ganti Pipa
@endsection
@section('content')



<a class="badge mb-3" href="{{route('menuLayanan')}}"><i class="fas fa-folder"></i> Ke Menu Layanan</a>
<div class="row">
    @if (Auth::user()->role != 'user')
    <div class="col-md-12 col-lg-12 col-12">
        <div class="card shadow border-0">
            <div class="card-body">
                {{-- <h5>Keluhan yang sudah selesai </h5><br><br> --}}
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" width=" 100%"
                        cellspacing="0">
                        <thead style="">
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Nomor Handphone</th>
                                <th>Teknisi</th>
                                <th>Biaya</th>
                                <th>Model Pipa</th>
                                <th>Tanggal Keluhan</th>
                                <th>Tanggal Selesai</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($ganti_pipa as $item)
                            @php
                            $mulai = new DateTime($item->created_at);
                            $selesai = new DateTime($item->tanggal_selesai);
                            $interval = $mulai->diff($selesai);
                            @endphp
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->nama_pelanggan}}</td>
                                <td>{{$item->nik}}</td>
                                <td>{{$item->hp}}</td>
                                <td>{{$item->name}}</td>
                                <td>Rp. {{number_format($item->biaya)}}</td>
                                <td>{{$item->model}}</td>
                                <td>{{date('d-m-Y',strtotime($item->created_at))}}</td>
                                <td>
                                    <ul>
                                        <li><span class="font-weight-bold">Tanggal Selesai</span> :
                                            {{date('d-m-Y',strtotime($item->tanggal_selesai))}}</li>
                                        <li><span class="font-weight-bold">Jangka Waktu Penyelesaian</span> :
                                            {{$interval->days}} Hari</li>
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                    <a href="{{route('gantiPipaPrint')}}" class="badge" target="_blank">
                        <i class="fas fa-print"></i> Print Laporan
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>


@endsection