@extends('templates.main')
@section('title')
Update Keluhan
@endsection
@section('content')


@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a class="badge mb-3" href="{{route('pelanggan')}}"><i class="fas fa-folder"></i> Ke Tabel Keluhan</a>
<br>
<form action="{{route('ubahStatusKeluhan',$keluhan->id_keluhan)}}" method="POST">
    @method('patch')
    @csrf
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card shadow border-0">
                <div class="card-body">
                    <h3>{{$keluhan->nama_pelanggan}}</h3>
                    <h6>Keluhan : {{date('d-m-Y',strtotime($keluhan->created_at))}}</h6>
                    <div class="form-group">
                        <label for="nik">NIK Pelanggan</label>
                        <input type="text" name="nik" id="nik" class="form-control" value="{{$keluhan->nik}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="nik">Jenis Keluhan</label>
                        <input type="text" name="nik" id="nik" class="form-control" value="{{$keluhan->jenis_keluhan}}"
                            readonly>
                    </div>
                    @if ($keluhan->jenis_keluhan == 'Lainnya')
                    <div class="form-group">
                        <label for="nik">Keluhan</label>
                        <input type="text" name="nik" id="nik" class="form-control" value="{{$keluhan->keluhan}}"
                            readonly>
                    </div>
                    @endif

                    @if ($keluhan->jenis_keluhan == 'Ganti Pipa' | $keluhan->jenis_keluhan == 'Ganti Meter')
                    <div class="form-group">
                        <label for="biaya">Biaya</label>
                        <input type="number" name="biaya" id="biaya" class="form-control" value="{{$keluhan->biaya}}"
                            readonly>
                        @if ($errors->has('biaya'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('biaya') }}</strong>
                        </span>
                        @endif
                    </div>
                    @endif

                    @if ($keluhan->jenis_keluhan == 'Ganti Meter' | $keluhan->jenis_keluhan == 'Tera Meter')
                    <div class="form-group">
                        <label for="tgl_pemeriksaan_perbaikan">Tanggal Perbaikan / Pemeriksaan</label>
                        <input type="date" name="tgl_pemeriksaan_perbaikan" id="tgl_pemeriksaan_perbaikan"
                            class="form-control" value="{{date('Y-m-d')}}">
                    </div>
                    @endif

                    @if ($keluhan->jenis_keluhan == 'Tera Meter')
                    <div class="form-group">
                        <label for="hasil_pemeriksaan">Hasil Pemeriksaan</label>
                        <input type="text" name="hasil_pemeriksaan" id="hasil_pemeriksaan" class="form-control">
                    </div>
                    @endif

                    <div class="form-group">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i> Keluhan Sudah di Tangani</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection



@section('script')
<script>
    $('#nik').on('input',function(){
            const nik = $('#nik').val();
            $('#nama-pelanggan').html('');
            $('#id_pelanggan').val('');
            $.ajax({
                url:"{{url('autokomplit-pelanggan')}}",
                type:'get',
                data:{nik:nik},
                success:function(data){
                    const json = JSON.parse(data);

                    if(json){
                        $('#nama-pelanggan').html(json.nama_pelanggan);
                        $('#id_pelanggan').val(json.id_pelanggan);
                    }else{
                        $('#nama-pelanggan').html("<span class='text-danger'>NIK tidak terdaftar<span>");
                    }
                }
            });
            
        });
</script>
@endsection