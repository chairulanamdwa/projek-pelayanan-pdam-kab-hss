@extends('templates.print')
@section('title','Data Pelanggan')

@section('content')
    
        <table class="table table-striped table-bordered" >
            <thead style="">
                <tr>
                    <th>#</th>
                    <th>No Pelanggan</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Urutan</th>
                    <th>Meter Lalu</th>
                    <th>Catat Meter</th>
                    <th>Gol.Tarif</th>
                    <th>Ukuran Pipa</th>
                    <th>Sewa Meter</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pelanggan as $item)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->no_pelanggan}}</td>
                    <td>{{$item->nama_pelanggan}}</td>
                    <td>{{$item->alamat}}</td>
                    <td>{{$item->urutan}}</td>
                    <td>{{$item->meter_lalu}}</td>
                    <td>{{$item->catat_meter}}</td>
                    <td>{{$item->kode}}</td>
                    <td>{{$item->ukuran_pipa}}</td>
                    <td>{{$item->sewa_meter}}</td>
                    <td>{{$item->status}}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
@endsection