@extends('templates.main')
@section('title','Pemasangan Baru / Tambah Pelanggan')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a href="{{url('pelanggan')}}" class="badge" style="margin-right:20px"><i class="fa fa-arrow-left"></i> Kembali</a>
<a href="{{url('tarif')}}" class="badge"><i class="fas fa-folder fa-fw"></i> Golongan Tarif</a>
<br><br>

<form action="{{url('pelanggan')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="nama_pelanggan">Nama</label>
                                <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control">
                                @if ($errors->has('nama_pelanggan'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('nama_pelanggan') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="nik">NIK</label>
                                <input type="text" name="nik" id="nik" class="form-control">
                                @if ($errors->has('nik'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('nik') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="id_wilayah">Wilayah</label>
                                <select type="text" name="id_wilayah" id="id_wilayah" class="form-control">
                                    <option value="">..Pilih</option>
                                    @foreach ($wilayah as $item)
                                    <option value="{{$item->id_wilayah}}">{{$item->nama_wilayah}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('id_wilayah'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('id_wilayah') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <textarea rows="3" name="alamat" id="alamat" class="form-control"></textarea>
                                @if ($errors->has('alamat'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('alamat') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="urutan">Urutan</label>
                                <input type="text" name="urutan" id="urutan" class="form-control">
                                @if ($errors->has('urutan'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('urutan') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="meter_lalu">Meter Lalu</label>
                                <input type="text" name="meter_lalu" id="meter_lalu" class="form-control">
                                @if ($errors->has('meter_lalu'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('meter_lalu') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="catat_meter">Catat Meter</label>
                                <input type="text" name="catat_meter" id="catat_meter" class="form-control">
                                @if ($errors->has('catat_meter'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('catat_meter') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="gol_tarif">Golongan Tarif</label>
                                <select type="text" name="gol_tarif" id="gol_tarif" class="form-control">
                                    <option value="">Kode</option>
                                    @foreach ($tarif as $item)
                                    <option value="{{$item->id_tarif}}">{{$item->kode}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('gol_tarif'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('gol_tarif') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="ukuran_pipa">Ukuran Pipa</label>
                                <input type="text" name="ukuran_pipa" id="ukuran_pipa" class="form-control">
                                @if ($errors->has('ukuran_pipa'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('ukuran_pipa') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="sewa_meter">Sewa Meter</label>
                                <input type="text" name="sewa_meter" id="sewa_meter" class="form-control">
                                @if ($errors->has('sewa_meter'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('sewa_meter') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label><br>
                                <input type="checkbox" name="status" id="status" value="1" checked>
                                @if ($errors->has('status'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection