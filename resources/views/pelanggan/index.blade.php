@extends('templates.main')
@section('title','Tabel Data Pelanggan')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
<br>

<a class="badge mb-3" href="{{route('menuLayanan')}}"><i class="fas fa-arrow-left"></i> Kembali</a>
<a href="{{url('pelanggan-tambah')}}" class=" btn btn-sm btn-primary">
    <i class="fas fa-plus"></i> Pemasangan
    Baru
</a>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow border-0">
            <div class="card-body">
                <table class="table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0">
                    <thead style="">
                        <tr>
                            <th>#</th>
                            <th><span class="text-danger">*</span></th>
                            <th>No Pelanggan</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Urutan</th>
                            <th>Meter Lalu</th>
                            <th>Catat Meter</th>
                            <th>Gol.Tarif</th>
                            <th>Ukuran Pipa</th>
                            <th>Sewa Meter</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pelanggan as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td class="text-center">
                                <form action="{{url('pelanggan').'/'.$item->id_pelanggan}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-xs text-danger" style="background:transparent"><i
                                            class="fa fa-times"></i></button>
                                </form>
                                <a href="{{url('pelanggan-edit').'/'.$item->id_pelanggan}}" class="text-success"><i
                                        class="fa fa-edit"></i></a>
                            </td>
                            <td>{{$item->no_pelanggan}}</td>
                            <td>{{$item->nama_pelanggan}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->urutan}}</td>
                            <td>{{$item->meter_lalu}}</td>
                            <td>{{$item->catat_meter}}</td>
                            <td>{{$item->kode}}</td>
                            <td>{{$item->ukuran_pipa}}</td>
                            <td>{{$item->sewa_meter}}</td>
                            <td>{{$item->status}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
                <a href="{{url('pelanggan-print')}}" class=" badge" target="_blank">
                    <i class="fas fa-print"></i> Cetak Laporan
                </a>
                <a href="{{url('tarif')}}" class=" btn btn-xs" target="_blank">
                    <img src="{{url('public/assets/img/ios-icon/photos.png')}}" width="20px" height="20px"
                        style="margin-left:10px"> Golongan Tarif
                </a>
            </div>
        </div>

    </div>

</div>
@endsection