@extends('templates.main')
@section('title')
Dashboard
@endsection

@section('content')

<div class="row">
    <div class="col-sm-6 col-md-3 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pelanggan</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$pelanggan}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Tera</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$tera}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Kebocoran</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$kebocoran}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-12 col-md-6">
        <div class="card shadow border-0">
            <div class="card-body">
                <h5>Analilis Pelanggan Berdasarkan Wilayah</h5>
                <div class="list-group">
                    <table class="table table-bordered table-hover">
                        @foreach ($pbwilayah as $item)
                        <tr>
                            <td>{{$item->nama_wilayah}}</td>
                            <td class="text-center"><span class="badge badge-primary">{{$item->jumlah_wil}}</span></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card shadow border-0">
            <div class="card-body">
                <h5>Wilayah yang sering mengalami Kerusakan/Keluhan</h5>
                <div class="list-group">
                    <table class="table table-bordered table-hover">
                        @foreach ($seringRusak as $item)
                        <tr>
                            <td>{{$item->nama_wilayah}}</td>
                            <td class="text-center"><span class="badge badge-primary">{{$item->jumlah_wil}}</span></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection