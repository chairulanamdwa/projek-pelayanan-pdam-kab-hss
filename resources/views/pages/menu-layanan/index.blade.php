@extends('templates.main')
@section('title')
Menu Data Laporan
@endsection
@section('content')

<style>
    .link-card {
        color: rgb(58, 58, 58);
    }

    .link-card:hover {
        text-decoration: none
    }
</style>

<div class="row">
    <div class="col-12 col-md-12">
        <div class="list-group  border-0">
            <a href="{{route('bukaTutupMeter')}}" class="list-group-item list-group-item-action border-0 shadow"><i
                    class="fas fa-folder fa-fw mr-2"></i> Buka dan Tutup
                Meter</a>
            <a href="{{route('gantiMeter')}}" class="list-group-item list-group-item-action border-0 shadow"><i
                    class="fas fa-folder fa-fw mr-2"></i>
                Ganti Meter</a>
            <a href="{{route('teraMeter')}}" class="list-group-item list-group-item-action border-0 shadow"><i
                    class="fas fa-folder fa-fw mr-2"></i>
                Tera Meter</a>
            <a href="{{route('kebocoran')}}" class="list-group-item list-group-item-action border-0 shadow"><i
                    class="fas fa-folder fa-fw mr-2"></i>
                Kebocoran</a>
            <a href="{{route('tunggakanRekAir')}}" class="list-group-item list-group-item-action border-0 shadow"><i
                    class="fas fa-folder fa-fw mr-2"></i>
                Tunggakan Rekening Air</a>
            <a href="{{route('gantiPipa')}}" class="list-group-item list-group-item-action border-0 shadow"><i
                    class="fas fa-folder fa-fw mr-2"></i>
                Ganti Pipa</a>
            <a href="{{route('kinerjaTeknisi')}}" class="list-group-item list-group-item-action border-0 shadow"><i
                    class="fas fa-folder fa-fw mr-2"></i>
                Laporan Kinerja Teknisi
            </a>
        </div>
    </div>
</div>

{{-- 
<div class="row">
    <div class="col-6 col-md-4 my-2">
        <div class="card border-0 shadow">
            <div class="card-body">
                <a href="{{route('bukaTutupMeter')}}" class='link-card'><i
    class="fas fa-box-open fa-fw mr-2 text-success"></i>
Buka Tutup
Meter</a>
</div>
</div>
</div>
<div class="col-6 col-md-4 my-2">
    <div class="card border-0 shadow">
        <div class="card-body">
            <a href="{{route('tera')}}" class='link-card'>
                <i class="fas fa-exchange-alt fa-fw mr-2 text-success"></i> Ganti Meter
            </a>
        </div>
    </div>
</div>
<div class="col-6 col-md-4 my-2">
    <div class="card border-0 shadow">
        <div class="card-body">
            <a href="{{route('kebocoran')}}" class='link-card'>
                <i class="fas fa-tape fa-fw mr-2 text-success"></i> Kebocoran
            </a>
        </div>
    </div>
</div>
<div class="col-6 col-md-4 my-2">
    <div class="card border-0 shadow">
        <div class="card-body">
            <a href="{{route('pelanggan')}}" class='link-card'>
                <i class="fas fa-tools fa-fw mr-2 text-success"></i> Pelanggan
            </a>
        </div>
    </div>
</div>
<div class="col-6 col-md-4 my-2">
    <div class="card border-0 shadow">
        <div class="card-body">
            <a href="{{route('tunggakanRekAir')}}" class='link-card'>
                <i class="fas fa-file-invoice-dollar fa-fw mr-2 text-success"></i> Tunggakan Rekening Air
            </a>
        </div>
    </div>
</div>
</div> --}}


@endsection