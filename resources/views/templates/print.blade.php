<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   
    <link href="{{url('public/assets')}}/css/bootstrap.min.css" rel="stylesheet">
    <title>Laporan @yield('title')</title>
    <style>
        .line-kop{
            border:1px solid #222;
        }
    </style>
</head>
<body>

    <div class="row text-center" style="display:flex;align-items:center">
        <div class="col-xs-2">
            <img src="{{url('public/assets/img/logo-hss.png')}}" width="70px" height="80px" alt="">
        </div>
        <div class="col-xs-8">
            <h4>PEMERINTAH KABUPATEN HULU SUNGAI SELATAN</h4>
            <h4>PERUSAHAAN DAERAH AIR MINUM</h4>
            <p>Jl.Kamboja No.1 RT.4 RK.V Telp.(0517)21290 Fax(0517)21403 Kandangan- 71212</p>

        </div>
        <div class="col-xs-2">
            <img src="{{url('public/assets/img/PDAM.png')}}" width="70px" height="80px" alt="">
        </div>
    </div>
    <div class="line-kop"></div>

    <h4 class="text-center">Laporan @yield('title')</h4>
    <br>

    @yield('content')
    <script>
        window.print();
    </script>
</body>
</html>