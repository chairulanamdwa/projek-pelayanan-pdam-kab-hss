@include('templates.includes.header')
@include('templates.includes.sidebar')
<!-- Main Content -->
<div id="content">
    @include('templates.includes.top')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">@yield('title')</h1>

        @yield('content')
    </div>
    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@include('templates.includes.footer')