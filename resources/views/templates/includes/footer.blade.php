<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; PDAM Kabupaten Hulu Sungai Selatan {{date('Y')}}</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

<!-- Bootstrap core JavaScript-->
<script src="{{url('public/templates/backend')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{url('public/templates/backend')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="{{url('public/templates/backend')}}/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="{{url('public/templates/backend')}}/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="{{url('public/templates/backend')}}/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('public/templates/backend')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="{{url('public/templates/backend')}}/js/demo/datatables-demo.js"></script>


@yield('script')

</body>

</html>