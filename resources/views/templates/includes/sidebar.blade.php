<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-secondary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
            <img src="{{url('public/assets/img/PDAM.png')}}" alt="PDAM Kabupaten Hulu Sungai Selatan" width="50">
        </div>
        <div class="sidebar-brand-text ">PDAM KABUPATEN HSS</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    @if (Auth::user()->role != 'user')
    <li class="nav-item  @if ($sidebar == 'Dashboard') active @endif">
        <a class="nav-link pb-0" href="{{route('dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    @endif

    @if (Auth::user()->role == 'admin' || Auth::user()->role == 'super admin')
    <li class="nav-item  @if ($sidebar == 'menu') active @endif">
        <a class="nav-link  pb-0" href="{{route('menuLayanan')}}">
            <i class="fas fa-fw fa-list"></i>
            <span>Menu</span></a>
    </li>
    @endif

    <li class="nav-item  @if ($sidebar == 'keluhan') active @endif">
        @if (Auth::user()->role == 'user')
        <a class="nav-link pb-0" href="{{route('keluhanAdd')}}">
            <i class="fas fa-fw fa-exclamation-triangle"></i>
            <span>Keluhan</span></a>
        @else
        <a class="nav-link pb-0" href="{{route('keluhan')}}">
            <i class="fas fa-fw fa-exclamation-triangle"></i>
            <span>Keluhan</span></a>
        @endif
    </li>

    @if (Auth::user()->role == 'admin')

    <li class="nav-item  @if ($sidebar == 'wilayah') active @endif">
        <a class="nav-link pb-0" href="{{route('wilayah')}}">
            <i class="fas fa-fw fa-map"></i>
            <span>Wilayah</span></a>
    </li>

    <li class="nav-item @if ($sidebar == 'data master') active @endif">
        <a class="nav-link collapsed " href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-database"></i>
            <span>Data Master</span>
        </a>
        <div id="collapseUtilities" class="collapse @if ($sidebar == 'data master') show @endif"
            aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tabels Data Master:</h6>
                <a class="collapse-item @if ($title == 'pelanggan') active @endif"
                    href="{{route('pelanggan')}}">Pelanggan</a>
                <a class="collapse-item @if ($title == 'tarif') active @endif" href="{{route('tarif')}}">Golongan
                    Tarif</a>
                <a class="collapse-item @if ($title == 'akun user') active @endif" href="{{route('users')}}">Akun
                    User</a>
            </div>
        </div>
    </li>
    @endif

    <!-- Divider -->
    <hr class="sidebar-divider">


</ul>
<!-- End of Sidebar -->
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">