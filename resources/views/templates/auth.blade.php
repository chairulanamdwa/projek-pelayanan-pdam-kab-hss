<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PDAM | @yield('title')</title>

    <link href="{{url('public/assets')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('public/assets')}}/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="{{url('public/assets')}}/css/animate.css" rel="stylesheet">
    <link href="{{url('public/assets')}}/css/style.css" rel="stylesheet">

    <style>
        body {
            background-image: url('public/assets/img/gallery/4.jpg');
            background-repeat: no-repeat;
            background-size: cover;
        }

        body::after {
            content: "";
            z-index: -99;
            background: rgba(0, 0, 0, .8);
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
        }
    </style>


</head>

<body>

    <div class="loginColumns animated fadeInDown">
        <div class="row" style="justify-content:center;display:flex">
            <div class="col-md-12">
                <div class="ibox-content" style="background:none;border:0;">
                    <h2 style="font-weight:bold" class="text-white text-center">
                        <img src="{{url('public/assets/img/PDAM.png')}}" alt="PDAM Kabupaten Hulu Sungai Selatan"
                            width="50">
                        PDAM KABUPATEN HULU SUNGAI SELATAN </h2> <br><br>
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

</body>

</html>