@extends('templates.main')
@section('title','Buka Tutup Meter')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a href="{{url('buka-tutup-meter')}}" class="badge" style="margin-right:20px"><i class="fa fa-arrow-left"></i>
    Kembali</a>
<br><br>

<form action="{{url('buka-tutup-meter')}}" method="POST">
    @csrf

    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card shadow border-0">
                <div class="card-body">
                    <h3><span id="nama-pelanggan"></span></h3>
                    <div class="form-group">
                        <label for="nik">NIK Pelanggan</label>
                        <input type="text" name="nik" id="nik" class="form-control">
                        <input type="hidden" name="id_pelanggan" id="id_pelanggan" class="form-control">
                        @if ($errors->has('id_pelanggan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('id_pelanggan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="no_ds">Nomor DS</label>
                        <input type="text" name="no_ds" id="no_ds" class="form-control">
                        @if ($errors->has('no_ds'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('no_ds') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="permintaan">Permintaan</label>
                        <input type="text" name="permintaan" id="permintaan" class="form-control">
                        @if ($errors->has('permintaan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('permintaan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tanggal">Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal" class="form-control" value="{{date('Y-m-d')}}">
                        @if ($errors->has('tanggal'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tanggal') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="aktif">Aktif</label>
                        <input type="checkbox" name="aktif" id="aktif" value="1" checked>
                        @if ($errors->has('aktif'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('aktif') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i> Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    $('#nik').on('input',function(){
            const nik = $('#nik').val();
            $('#nama-pelanggan').html('');
            $('#id_pelanggan').val('');
            $.ajax({
                url:"{{url('autokomplit-pelanggan')}}",
                type:'get',
                data:{nik:nik},
                success:function(data){
                    const json = JSON.parse(data);
                    if(json){
                        $('#nama-pelanggan').html(json.nama_pelanggan);
                        $('#id_pelanggan').val(json.id_pelanggan);
                    }else{
                        $('#nama-pelanggan').html("<span class='text-danger'>NIK tidak terdaftar<span>");
                    }
                }
            });
            
        });
</script>
@endsection