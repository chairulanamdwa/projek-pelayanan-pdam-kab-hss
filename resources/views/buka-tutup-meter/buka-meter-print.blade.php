@extends('templates.print')
@section('title')
Buka Meter
@endsection

@section('content')

<table class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead style="">
        <tr>
            <th>#</th>
            <th>No DS</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Permintaan</th>
            <th>Tanggal</th>
        </tr>
    </thead>

    <tbody id="tabeldata">
        @foreach ($buka as $item)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$item->no_ds}}</td>
            <td>{{$item->nama_pelanggan}}</td>
            <td>{{$item->alamat}}</td>
            <td>{{$item->permintaan}}</td>
            <td>{{date('d/m/Y',strtotime($item->tanggal))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection