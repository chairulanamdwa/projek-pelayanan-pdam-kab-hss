@extends('templates.main')
@section('title','Edit Data Buka Tutup Meter')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a href="{{url('buka-tutup-meter')}}" class="badge" style="margin-right:20px"><i class="fa fa-arrow-left"></i>
    Kembali</a>
<br><br>

<form action="{{url('buka-tutup-meter').'/'.$buktup->id_buktup_meter}}" method="POST">
    @method('patch')
    @csrf

    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card border-0 shadow">
                <div class="card-body">

                    <h3><span id="nama-pelanggan">{{$buktup->nama_pelanggan}}</span></h3>
                    <div class="form-group">
                        <label for="no_ds">Nomor DS</label>
                        <input type="text" name="no_ds" id="no_ds" class="form-control" value="{{$buktup->no_ds}}">
                        @if ($errors->has('no_ds'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('no_ds') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="permintaan">Permintaan</label>
                        <input type="text" name="permintaan" id="permintaan" class="form-control"
                            value="{{$buktup->permintaan}}">
                        @if ($errors->has('permintaan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('permintaan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tanggal">Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal" class="form-control"
                            value="{{date('Y-m-d',strtotime($buktup->tanggal))}}">
                        @if ($errors->has('tanggal'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tanggal') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="aktif">Aktif</label>
                        <input type="checkbox" name="aktif" id="aktif" value="1" @if ($buktup->aktif == 1) checked
                        @endif>
                        @if ($errors->has('aktif'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('aktif') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i> Edit Data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    $('#nik').on('input',function(){
            const nik = $('#nik').val();
            $('#nama-pelanggan').html('');
            $.ajax({
                url:"{{url('autokomplit-pelanggan')}}",
                type:'get',
                data:{nik:nik},
                success:function(data){
                    const json = JSON.parse(data);

                    if(json){
                        $('#nama-pelanggan').html(json.nama_pelanggan);
                        $('#id_pelanggan').val(json.id_pelanggan);
                    }else{
                        $('#nama-pelanggan').html("<span class='text-danger'>NIK tidak terdaftar<span>");
                    }
                }
            });
            
        });
</script>
@endsection