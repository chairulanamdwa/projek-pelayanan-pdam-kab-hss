@extends('templates.main')
@section('title')
Buka Tutup Meter
@endsection
@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a class="badge mb-3" href="{{route('menuLayanan')}}"><i class="fas fa-arrow-left"></i> Kembali</a>

<a href="{{url('buka-tutup-meter-tambah')}}" class="btn btn-sm btn-primary">
    <i class="fas fa-plus"></i> Buka Meter Baru
</a>
<br>
<br>
<div class="row">
    <div class="col-md-6">
        <div class="card shadow border-0">
            <div class="card-body">
                <h4>Buka Meter</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" width="100%"
                        cellspacing="0">
                        <thead style="">
                            <tr>
                                <th>#</th>
                                <th><span class="text-danger">*</span></th>
                                <th>No DS</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Permintaan</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>

                        <tbody id="tabeldata">
                            @foreach ($buka as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td class="text-center">
                                    <form action="{{url('buka-tutup-meter').'/'.$item->id_buktup_meter}}" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button onclick="return confirm('Apakah anda yakin ingin menghapus')"
                                            class="btn btn-xs text-danger" style="background:transparent"><i
                                                class="fa fa-times f-fw"></i></button>
                                    </form>
                                    <a href="{{url('buka-tutup-meter-edit').'/'.$item->id_buktup_meter}}"
                                        class="text-success"><i class="fas fa-edit f-fw"></i></a>
                                </td>
                                <td>{{$item->no_ds}}</td>
                                <td>{{$item->nama_pelanggan}}</td>
                                <td>{{$item->alamat}}</td>
                                <td>{{$item->permintaan}}</td>
                                <td>{{date('d/m/Y',strtotime($item->tanggal))}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <br>
                <a href="{{route('BukaMeterPrint')}}" target="_blank" class="badge">
                    Print Laporan <i class="fas fa-print"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow border-0">
            <div class="card-body">
                <h4>Tutup Meter</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" width="100%"
                        cellspacing="0">
                        <thead style="">
                            <tr>
                                <th>#</th>
                                <th><span class="text-danger">*</span></th>
                                <th>No DS</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Permintaan</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>

                        <tbody id="tabeldata">
                            @foreach ($tutup as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td class="text-center">
                                    <form action="{{url('buka-tutup-meter').'/'.$item->id_buktup_meter}}" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button onclick="return confirm('Apakah anda yakin ingin menghapus')"
                                            class="btn btn-xs text-danger" style="background:transparent"><i
                                                class="fa fa-times f-fw"></i></button>
                                    </form>
                                    <a href="{{url('buka-tutup-meter-edit').'/'.$item->id_buktup_meter}}"
                                        class="text-success"><i class="fas fa-edit f-fw"></i></a>
                                </td>
                                <td>{{$item->no_ds}}</td>
                                <td>{{$item->nama_pelanggan}}</td>
                                <td>{{$item->alamat}}</td>
                                <td>{{$item->permintaan}}</td>
                                <td>{{date('d/m/Y',strtotime($item->tanggal))}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <br>
                <a href="{{route('TutupMeterPrint')}}" target="_blank" class="badge">
                    Print Laporan <i class="fas fa-print"></i>
                </a>
            </div>
        </div>
    </div>


</div>

@endsection