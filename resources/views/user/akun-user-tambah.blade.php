@extends('templates.main')
@section('title','Buat Akun User Baru')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<br>

<a href="{{route('users')}}" class="badge mb-2"><i class="fas fa-arrow-left fw-fw"></i> Kembali</a>

<form action="{{ url('akun-user') }}" method="POST">
    @csrf

    <h3><span id="nama-pelanggan"></span></h3>
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama Lengkap</label>
                        <input type="text" name="name" id="name" class="form-control">
                        @if ($errors->has('name'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control">
                        @if ($errors->has('email'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        @if (Auth::user()->role == 'super admin')
                        <label for="Super Admin">
                            <input type="radio" name="role" id="Super Admin" value="super admin"> Super Admin
                        </label>
                        @endif
                        <label for="admin">
                            <input type="radio" name="role" id="admin" value="admin"> admin
                        </label>
                        <label for="teknisi">
                            <input type="radio" name="role" id="teknisi" value="teknisi"> Teknisi
                        </label>
                        <label for="user">
                            <input type="radio" name="role" id="user" value="user"> User
                        </label>
                        <br>
                        @if ($errors->has('role'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('role') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" class="form-control">
                        @if ($errors->has('password'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i> Buat Laporan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    $('#nik').on('input',function(){
            const nik = $('#nik').val();
            $('#nama-pelanggan').html('');
            $('#id_pelanggan').val('');
            $.ajax({
                url:"{{url('autokomplit-pelanggan')}}",
                type:'get',
                data:{nik:nik},
                success:function(data){
                    const json = JSON.parse(data);

                    if(json){
                        $('#nama-pelanggan').html(json.nama_pelanggan);
                        $('#id_pelanggan').val(json.id_pelanggan);
                    }else{
                        $('#nama-pelanggan').html("<span class='text-danger'>NIK tidak terdaftar<span>");
                    }
                }
            });
            
        });
</script>
@endsection