@extends('templates.main')
@section('title','Pengaturan Profil')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<br>

<div class="mb-4">
    <a href="{{route('dashboard')}}" class="badge"><i class="fas fa-folder fa-fw"></i> Ke Dashboard</a>
    <a href="{{route('dashboard')}}" class="badge"><i class="fas fa-folder fa-fw"></i> Ke Menu Pelayanan</a>
</div>

<form action="{{url('ganti-profil')}}" method="POST" enctype="multipart/form-data" class="">
    @method('patch')
    @csrf

    <h3><span id="nama-pelanggan"></span></h3>
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama Lengkap</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{Auth::user()->name}}">
                        @if ($errors->has('name'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="gambar">Foto</label>
                        <input type="file" name="gambar" id="gambar" class="form-control">
                        @if ($errors->has('gambar'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('gambar') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i> Perbarui</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    $('#nik').on('input',function(){
            const nik = $('#nik').val();
            $('#nama-pelanggan').html('');
            $('#id_pelanggan').val('');
            $.ajax({
                url:"{{url('autokomplit-pelanggan')}}",
                type:'get',
                data:{nik:nik},
                success:function(data){
                    const json = JSON.parse(data);

                    if(json){
                        $('#nama-pelanggan').html(json.nama_pelanggan);
                        $('#id_pelanggan').val(json.id_pelanggan);
                    }else{
                        $('#nama-pelanggan').html("<span class='text-danger'>NIK tidak terdaftar<span>");
                    }
                }
            });
            
        });
</script>
@endsection