@extends('templates.main')
@section('title1')
List Akun User
@endsection
@section('title2','Tabel User')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<div class="row" style="background:white;padding:20px;border-radius:5px;">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover " id="dataTable" width="100%" cellspacing="0">
                <thead style="">
                    <tr>
                        <th>#</th>
                        <th class="text-center"><span class="text-danger">*</span></th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Role</th>
                        <th class="text-center">Gambar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td class="text-center">
                            @if ($item->role == 'admin' | $item->role == 'user' | $item->role == 'teknisi')
                            <form action="{{url('akun-user').'/'.$item->id}}" method="POST">
                                @method('delete')
                                @csrf
                                <button class="btn btn-xs text-danger" style="background:transparent"><i
                                        class="fa fa-times"></i></button>
                            </form>
                            @endif
                            <a href="{{url('akun-user-edit').'/'.$item->id}}" class="text-success"><i
                                    class="fa fa-edit"></i></a>
                        </td>
                        <td class="text-center">{{$item->name}}</td>
                        <td class="text-center">{{$item->role}}</td>
                        <td class="text-center"><img src="{{url('public/assets/img/user').'/'.$item->gambar}}"
                                alt="{{$item->name}}" width="50"></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <a href="{{url('akun-user-tambah')}}" class=" btn btn-xs" style="background:#ddd;color:black;font-weight:bold">
            <img src="{{url('public/assets/img/ios-icon/users.png')}}" width="20px" height="20px"> Buat Akun User
        </a>
        {{-- <a href="{{url('kebocoran-print')}}" class=" btn btn-xs" target="_blank"
        style="background:#ddd;color:black;font-weight:bold">
        <img src="{{url('public/assets/img/ios-icon/pages.png')}}" width="20px" height="20px"> Cetak Laporan
        </a> --}}
    </div>
</div>

@endsection