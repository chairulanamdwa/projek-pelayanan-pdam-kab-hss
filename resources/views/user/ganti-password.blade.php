@extends('templates.main')
@section('title','Ganti Password')

@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<br>

<div class="mb-4">
    <a href="{{route('dashboard')}}" class="badge"><i class="fas fa-folder fa-fw"></i> Ke Dashboard</a>
    <a href="{{route('dashboard')}}" class="badge"><i class="fas fa-folder fa-fw"></i> Ke Menu Pelayanan</a>
</div>

<form action="{{url('ganti-password')}}" method="POST">
    @method('patch')
    @csrf

    <h3><span id="nama-pelanggan"></span></h3>
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="old_password">Password Lama</label>
                        <input type="password" name="old_password" id="old_password" class="form-control"
                            value="{{Auth::user()->old_password}}">
                        @if ($errors->has('old_password'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('old_password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="new_password">Password Baru</label>
                        <input type="password" name="new_password" id="new_password" class="form-control"
                            value="{{Auth::user()->new_password}}">
                        @if ($errors->has('new_password'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('new_password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="konfirmasi">Password Konfirmasi</label>
                        <input type="password" name="konfirmasi" id="konfirmasi" class="form-control"
                            value="{{Auth::user()->konfirmasi}}">
                        @if ($errors->has('konfirmasi'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('konfirmasi') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"><i class="fa fa-pencil"></i> Ganti Password</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection