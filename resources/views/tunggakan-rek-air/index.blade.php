@extends('templates.main')
@section('title')

Laporan Tunggakan Rekening Air
@endsection
@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<a class="badge mb-3" href="{{route('menuLayanan')}}"><i class="fas fa-arrow-left"></i> Kembali</a>
<a href="{{url('tunggakan-rek-air-tambah')}}" class="btn btn-sm btn-primary">
    <i class="fas fa-plus"></i> Tambah
    Tunggakan
    Rek Air
</a>
<br>
<div class="row" style="background:white;padding:20px;border-radius:5px;">
    <div class="col-lg-12">
        <div class="card shadow border-0">
            <div class="card-body">
                <table class="table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0">
                    <thead style="">
                        <tr>
                            <th>#</th>
                            <th><span class="text-danger">*</span></th>
                            <th>No DS</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Tunggakan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tunggakan as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td class="text-center">
                                <form action="{{url('tunggakan-rek-air').'/'.$item->id_tunggakan}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button onclick="return confirm('Apakah anda yakin ingin menghapus?')"
                                        class="btn btn-xs text-danger" style="background:transparent"><i
                                            class="fa fa-times"></i></button>
                                </form>
                                <a href="{{url('tunggakan-rek-air-edit').'/'.$item->id_tunggakan}}"
                                    class="text-success"><i class="fa fa-edit"></i></a>
                            </td>
                            <td>{{$item->no_ds}}</td>
                            <td>{{$item->nama_pelanggan}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->tunggakan}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <a href="{{url('tunggakan-rek-air-print')}}" class="badge" target="_blank">
                    <i class="fas fa-print"></i> Cetak Laporan
                </a>
            </div>
        </div>
    </div>
</div>

@endsection